<?php ($_SERVER['HTTP_HOST']=='redot.global')?$ini_array = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/config/path.ini'):$ini_array = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/config/path.ini'); ?> 
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $pageTitle ?></title>
    <meta name="description" content="<?php echo $pageMetaDescription ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href=<?= $ini_array['path']."assets/css/main.css" ?> />
    <link rel="stylesheet" type="text/css" media="screen" href=<?= $ini_array['path']."assets/css/responsive.css"?> />
    <link rel="stylesheet" type="text/css" media="screen" href=<?= $ini_array['path']."assets/css/aos.css"?> />
    <link rel="stylesheet" type="text/css" href=<?= $ini_array['path']."assets/web-fonts-with-css/css/fontawesome-all.css"?>>
    <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="icon" href=<?= $ini_array['path']."assets/images/favicon.ico"?> sizes="32x32" />
    <link rel="icon" href="https://redotlabs.com/wp-content/uploads/2017/08/cropped-fav-redot-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://redotlabs.com/wp-content/uploads/2017/08/cropped-fav-redot-180x180.png" />
    <meta name="msapplication-TileImage" content="https://redotlabs.com/wp-content/uploads/2017/08/cropped-fav-redot-270x270.png" />
    <meta content="Redot" property="og:site_name" />
    <meta content=<?= $ini_array['path']."assets/images/og-redot.jpg"?> property="og:image" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-150518098-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-150518098-1');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P4QZ25P');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P4QZ25P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Load Facebook SDK for JavaScript -->
<!--<div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v5.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      Your customer chat code -->
      <!--<div class="fb-customerchat"
        attribution=setup_tool
        page_id="496898817137274"
		    theme_color="#cd2122">
      </div>-->