<footer class="footer">
            <div class="container">
                    <div class="col-sm-12 col-md-9">
					    <div class="row">
							<nav class=" footer__links">
								<ul id="menu-footer-1" class="list-unstyled col-md-3 col-sm-3 col-xs-6">
									<li id="menu-item-2822" class="two-column menu-item menu-item-type-post_type_archive menu-item-object-service menu-item-has-children menu-item-2822">Services
										<ul class="sub-menu">
											<li id="menu-item-2825" class="menu-item menu-item-type-post_type menu-item-object-service menu-item-2825"><a href=<?= $ini_array['path']."services/hiveminds"?>>HiveMinds</a></li>
											<li id="menu-item-2824" class="menu-item menu-item-type-post_type menu-item-object-service menu-item-2824"><a href=<?= $ini_array['path']."services/web-design-and-development"?>>Web Design and Development</a></li>
											<li id="menu-item-3565" class="menu-item menu-item-type-post_type menu-item-object-service menu-item-3565"><a href=<?= $ini_array['path']."services/ecommerce-development"?>>Ecommerce Development</a></li>
											<li id="menu-item-3566" class="menu-item menu-item-type-post_type menu-item-object-service menu-item-3566"><a href=<?= $ini_array['path']."services/mobile-apps"?>>Mobile Apps</a></li>
											<li id="menu-item-2819" class="menu-item menu-item-type-taxonomy menu-item-object-services menu-item-2819"><a href=<?= $ini_array['path']."services/system-integration"?>>System Integration</a></li>
											<li id="menu-item-2828" class="menu-item menu-item-type-post_type menu-item-object-service menu-item-2828"><a href=<?= $ini_array['path']."services/managed-cloud-services"?>>Managed Cloud Services</a></li>
											<li id="menu-item-2823" class="menu-item menu-item-type-post_type menu-item-object-service menu-item-2823"><a href=<?= $ini_array['path']."services/digital-marketing"?>>Digital Marketing</a></li>
										</ul>
									</li>
								</ul>
								<ul id="menu-footer-2" class="list-unstyled col-md-2 col-sm-3 col-xs-6">
									<li id="menu-item-2829" class="menu-item menu-item-type-post_type_archive menu-item-object-technology menu-item-has-children menu-item-2829">Technologies
										<ul class="sub-menu">
											<li id="menu-item-3558" class="menu-item menu-item-type-post_type menu-item-object-technology menu-item-3558"><a href=<?= $ini_array['path']."technologies/cms"?>>CMS</a></li>
											<li id="menu-item-3559" class="menu-item menu-item-type-post_type menu-item-object-technology menu-item-3559"><a href=<?= $ini_array['path']."technologies/ecommerce"?>>Ecommerce</a></li>
											<li id="menu-item-3564" class="menu-item menu-item-type-post_type menu-item-object-technology menu-item-3564"><a href=<?= $ini_array['path']."technologies/mobile"?>>Mobile</a></li>
											<li id="menu-item-3560" class="menu-item menu-item-type-post_type menu-item-object-technology menu-item-3560"><a href=<?= $ini_array['path']."technologies/cloud-service-providers"?>>Cloud Service Providers</a></li>
											<li id="menu-item-3561" class="menu-item menu-item-type-post_type menu-item-object-technology menu-item-3561"><a href=<?= $ini_array['path']."technologies/php"?>>PHP</a></li>
											<li id="menu-item-3562" class="menu-item menu-item-type-post_type menu-item-object-technology menu-item-3562"><a href=<?= $ini_array['path']."technologies/.net-development"?>>.Net Development</a></li>
											<li id="menu-item-2830" class="menu-item menu-item-type-taxonomy menu-item-object-technologies menu-item-2830"><a href=<?= $ini_array['path']."technologies/javascript"?>>Java Script</a></li>
										</ul>
									</li>
								</ul>
								<ul id="menu-footer-3" class="list-unstyled col-md-2 col-sm-12 col-xs-12">
										<li id="menu-item-3557" class="menu-item menu-item-type-post_type menu-item-object-service menu-item-3557"><a href=<?= $ini_array['path']."portfolio"?>>Portfolio</a></li>						
								</ul>
								<!--<ul id="menu-footer-4" class="list-unstyled col-md-2 col-sm-3 col-xs-6">
									<li id="menu-item-3373" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3373">Pricing
										<ul class="sub-menu">
											<li id="menu-item-3374" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3374"><a href=<?= $ini_array['path']."pricing/website"?>>Website</a></li>
											<li id="menu-item-3375" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3375"><a href=<?= $ini_array['path']."pricing/smm"?>>Social Media Marketing</a></li>
											<li id="menu-item-3375" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3376"><a href=<?= $ini_array['path']."pricing/seo"?>>Search Engine Optimization</a></li>
										</ul>
									</li>
						
								</ul>-->
								<ul id="menu-footer-5" class="list-unstyled col-md-2 col-sm-3 col-xs-6">
									<li id="menu-item-2842" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2842">About Us
										<ul class="sub-menu">
											<li id="menu-item-2839" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2839"><a href=<?= $ini_array['path']."about/company"?>>Company</a></li>
											<li id="menu-item-2840" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2840"><a href=<?= $ini_array['path']."about/management"?>>Management</a></li>
											<li id="menu-item-2837" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2837"><a href=<?= $ini_array['path']."about/team"?>>Team</a></li>
											<li id="menu-item-2841" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2841"><a href=<?= $ini_array['path']."about/life"?>>Life at Redot</a></li>
											<li id="menu-item-2838" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2838"><a href=<?= $ini_array['path']."about/careers"?>>Careers</a></li>
										</ul>
									</li>
								</ul>
								<ul class="list-unstyled col-md-3 col-sm-3 col-xs-6">
									<li>
										<a href=<?= $ini_array['path']."contact-us"?>>Contact Us</a>
										<ul class="sub-menu">
											<li><a class="tel" href="tel:+94112612627">+94 112 612 627</a></li>
											<li>No 03,
												<br> Sri Jinarathana Mawatha,
												<br> Rathmalana,
												<br> Sri Lanka.</li>
										</ul>
									</li>
								</ul>
							</nav>
						</div>
					</div>
                    <div class="col-sm-12 col-md-3 footer__right">
						<div class="row">
							<div class="social_links">
								<a href="https://www.facebook.com/redot.global/" target="_blank" class="social_link" alt="facebook">
									<i class="fab fa-facebook" aria-hidden="true"></i>
								</a>
								<a href="https://youtu.be/sw4gUo_i9eA" target="_blank" class="social_link" alt="youtube">
									<i class="fab fa-youtube-square" aria-hidden="true"></i>
								</a>
								<a href="https://www.instagram.com/redot.global" target="_blank" class="social_link" alt="intagram">
									<i class="fab fa-instagram" aria-hidden="true"></i>
								</a>
							</div>
							<div class="footer__bottom">
								<a href="/sitemap.xml" class="small-link">Sitemap</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" class="small-link">Privacy Policy</a>
								<img class="footer__partner-logo" src=<?= $ini_array['path']."assets/images/aws_partner_network.png"?> alt="aws network partner logo" style="width:131px; height:auto;"></div>
								<img class="footer__partner-logo" src=<?= $ini_array['path']."assets/images/swipecrypto_partner_logo.png"?> alt="swipecrypto logo" style="width:131px; height:auto;"></div>
								<img class="footer__partner-logo" src=<?= $ini_array['path']."assets/images/mediawize_partner_logo.png"?> alt="mediawize logo" style="width:131px; height:auto;"></div>
						</div>
					</div>
                </div>
        </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src=<?= $ini_array['path']."assets/js/parallax.min.js"?>></script>
	<script type="text/javascript" src=<?= $ini_array['path']."assets/js/aos.js"?>></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="//code.tidio.co/zmoe4p5xvmgtimdppahhf7bl5jf4zegu.js"></script>
	<script type="text/javascript" src=<?= $ini_array['path']."assets/js/jquery.flip.min.js"?>></script>
	<script type="text/javascript" src=<?= $ini_array['path']."assets/js/particles.js"?>></script>
	<!-- <script type="text/javascript" src=<?# $ini_array['path']."assets/js/app.js"?>></script> -->
    <script type="text/javascript" src=<?= $ini_array['path']."assets/js/main.js"?>></script>
    <!--<script>
		if($(".map").length>0)function initMap(){var e={lat:6.824832,lng:79.882487},n=new google.maps.Map(document.getElementById("map"),{zoom:15,center:e});new google.maps.Marker({position:e,map:n})}else function initMap(){}
    </script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmhBVwFZS4-WDSaOYYdlsIWvPr2N_T0dI&callback=initMap"></script>-->
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>

</html>