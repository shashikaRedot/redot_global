<?php ($_SERVER['HTTP_HOST']=='redot.global')?$ini_array = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/config/path.ini'):$ini_array = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/config/path.ini'); ?> 
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Welcome to Redot</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href=<?= $ini_array['path']."assets/css/main.css" ?> />
    <link rel="stylesheet" type="text/css" media="screen" href=<?= $ini_array['path']."assets/css/responsive.css"?> />
    <link rel="stylesheet" type="text/css" media="screen" href=<?= $ini_array['path']."assets/css/aos.css"?> />
    <link rel="stylesheet" type="text/css" href=<?= $ini_array['path']."assets/web-fonts-with-css/css/fontawesome-all.css"?>>
    <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="icon" href=<?= $ini_array['path']."assets/images/favicon.ico"?> sizes="32x32" />
    <link rel="icon" href="https://redotlabs.com/wp-content/uploads/2017/08/cropped-fav-redot-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://redotlabs.com/wp-content/uploads/2017/08/cropped-fav-redot-180x180.png" />
    <meta name="msapplication-TileImage" content="https://redotlabs.com/wp-content/uploads/2017/08/cropped-fav-redot-270x270.png" />
    <meta content="Redot" property="og:site_name" />
    <meta content=<?= $ini_array['path']."assets/images/og-redot.jpg"?> property="og:image" />
    <meta http-equiv=“Content-Security-Policy” content=”
   script-src ‘self’ ‘nonce-ch4hvvbHDpv7xCSvXCs3BrNggHdTzxUA’;
   frame-src ‘self’ ‘nonce-ch4hvvbHDpv7xCSvXCs3BrNggHdTzxUA’ ;
   style-src ‘self’ ‘unsafe-inline’;
   object-src ‘self’;
 “>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119399701-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-119399701-2');
    </script>
</head>
<body>