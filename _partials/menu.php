<?php $path = explode('/', $_SERVER['REQUEST_URI']);?>
<header>
    <div class="mod-header">
    <nav class="navbar navbar-fixed">
        <a href="<?= $ini_array['path']?>" class="navbar-brand" title="Redot Pvt LTD">
            <img src=<?= $ini_array['path']."assets/images/logo.svg" ?> alt="redot software solutions">
        </a>
        <div class="right-block">
            <div class="action-button">
                <a href=<?= $ini_array['path']."contact#contact-redot"?>>Get a quote</a>
            </div>
            <div class="menu-toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="navbar-menu">
            <ul class="main-menu">
                <li>
                    <a href="#" class="link">Services</a>
                    <div class="menu-body">
                        <ul>
                            <li><a href=<?= $ini_array['path']."services/hiveminds"?>>HiveMinds</a></li>
                            <li><a href=<?= $ini_array['path']."services/web-design-and-development"?>>web design and developement</a></li>
                            <li><a href=<?= $ini_array['path']."services/ecommerce-development"?>>ecommerce development</a></li>
                            <li><a href=<?= $ini_array['path']."services/mobile-apps"?>>mobile apps</a></li>
                            <li><a href=<?= $ini_array['path']."services/system-integration"?>>system integration</a></li>
                            <li><a href=<?= $ini_array['path']."services/managed-cloud-services"?>>managed cloud services</a></li>
                            <li><a href=<?= $ini_array['path']."services/digital-marketing"?>>digital marketing</a></li>
                            <!-- <li><a href=<?= $ini_array['path']."services/graphic-design"?>>graphic design</a></li> -->
                        </ul>
                    </div>
                </li>
                <li><a href=<?= $ini_array['path']."portfolio"?>>Portfolio</a></li>
                <li>
                    <a href="#" class="link">Technologies</a>
                    <div class="menu-body">
                        <ul>
                            <li><a href=<?= $ini_array['path']."technologies/cms"?>>cms</a></li>
                            <li><a href=<?= $ini_array['path']."technologies/ecommerce"?>>ecommerce</a></li>
                            <li><a href=<?= $ini_array['path']."technologies/mobile"?>>mobile</a></li>
                            <li><a href=<?= $ini_array['path']."technologies/cloud-service-providers"?>>cloud service providers</a></li>
                            <li><a href=<?= $ini_array['path']."technologies/php"?>>php</a></li>
                            <li><a href=<?= $ini_array['path']."technologies/.net-development"?>>.net developement</a></li>
                            <li><a href=<?= $ini_array['path']."technologies/javascript"?>>javascript</a></li>
                        </ul>
                    </div>
                </li>
                <!--<li>
                    <a href="#" class="link">Pricing</a>
                    <div class="menu-body">
                        <ul>
                            <li><a href=<?= $ini_array['path']."pricing/website"?>>website</a></li>
                            <li><a href=<?= $ini_array['path']."pricing/smm"?>>Social Media Marketing</a></li>
							<li><a href=<?= $ini_array['path']."pricing/seo"?>>Search Engine Optimization</a></li>
                        </ul>
                    </div>
                </li>-->
                <li>
                    <a href="#" class="link">About</a>
                    <div class="menu-body">
                        <ul>
                            <li><a href=<?= $ini_array['path']."about/company"?>>Company</a></li>
                            <li><a href=<?= $ini_array['path']."about/management"?>>Management</a></li>
                            <li><a href=<?= $ini_array['path']."about/team"?>>Team</a></li>
                            <li><a href=<?= $ini_array['path']."about/life"?>>Life at Redot</a></li>
                            <li><a href=<?= $ini_array['path']."about/careers"?>>Careers</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href=<?= $ini_array['path']."contact"?>>Contact</a></li>
            </ul>
        </div>
        <div class="mobile-menu" accesskey="m">
            <svg xmlns="http://www.w3.org/2000/svg" id="mobile-toggle" height="32" viewBox="0 0 32 32" width="32"><style>.a{fill:#121313;}</style><path d="M1 10h30c0.6 0 1-0.4 1-1 0-0.6-0.4-1-1-1H1C0.4 8 0 8.4 0 9 0 9.6 0.4 10 1 10z" class="a"/><path d="M31 15H1c-0.6 0-1 0.4-1 1 0 0.6 0.4 1 1 1h30c0.6 0 1-0.4 1-1C32 15.4 31.6 15 31 15z" class="a"/><path d="M31 22H11c-0.6 0-1 0.4-1 1s0.4 1 1 1h20c0.6 0 1-0.4 1-1S31.6 22 31 22z" class="a"/></svg>
        </div>

        <!--FULL MENU-->
        <div id="full-screen-menu" class="full-screen-menu full-screen-menu-collapse">
            <div class="navContainer">
                <div class="navWrap" tabindex="5000" style="overflow: hidden; outline: none;">
                    <ul class="nav-full">
                        <li class="nav-full-item active">
                            <a target="_self" class="<?= ($path[1]=='')?'active':'' ?>" href="<?= $ini_array['path']?>" title="Home">HOME</a>
                        </li>
                        <li class="nav-full-item parentNav">
                          <a class="mobileClass <?= ($path[1]=='services')?'active':'' ?>" target="_self" title="ABOUT">SERVICES</a>
                            <ul class="sub about-us">
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."services/hiveminds"?> class="<?= ($path[2]=='hiveminds')?'active':'' ?>"><div class="navText">HiveMinds</div></a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."services/web-design-and-development"?> class="<?= ($path[2]=='web-design-and-development')?'active':'' ?>">Web design and developement</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."services/ecommerce-development"?> class="<?= ($path[2]=='ecommerce-development')?'active':'' ?>">Ecommerce development</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."services/mobile-apps"?> class="<?= ($path[2]=='mobile-apps')?'active':'' ?>">Mobile apps</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."services/system-integration"?> class="<?= ($path[2]=='system-integration')?'active':'' ?>">System integration</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."services/managed-cloud-services"?> class="<?= ($path[2]=='managed-cloud-services')?'active':'' ?>">Managed cloud services</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."services/digital-marketing"?> class="<?= ($path[2]=='digital-marketing')?'active':'' ?>">Digital marketing</a></li>
                            </ul>
                        </li>
                        <li class="nav-full-item"> <a class="<?= ($path[1]=='portfolio')?'active':'' ?>" target="_self" href=<?= $ini_array['path']."portfolio"?> title="PORTFOLIO">PORTFOLIO</a></li>
                        <li class="nav-full-item parentNav">
                          <a class="mobileClass <?= ($path[1]=='technologies')?'active':'' ?>" target="_self"  title="Technologies">TECHNOLOGIES</a>
                            <ul class="sub technologies">
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."technologies/cms"?> class="<?= ($path[2]=='cms')?'active':'' ?>">CMS</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."technologies/ecommerce"?> class="<?= ($path[2]=='ecommerce')?'active':'' ?>">Ecommerce</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."technologies/mobile"?> class="<?= ($path[2]=='mobile')?'active':'' ?>">Mobile</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."technologies/cloud-service-providers"?> class="<?= ($path[2]=='cloud-service-providers')?'active':'' ?>">Cloud service providers</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."technologies/php"?> class="<?= ($path[2]=='php')?'active':'' ?>">PHP</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."technologies/.net-development"?> class="<?= ($path[2]=='.net-development')?'active':'' ?>">.Net developement</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."technologies/javascript"?> class="<?= ($path[2]=='javascript')?'active':'' ?>">Javascript</a></li>
                            </ul>
                        </li>
                        <!--<li class="nav-full-item parentNav">
                          <a class="mobileClass <?= ($path[1]=='pricing')?'active':'' ?>" target="_self"  title="Pricing">PRICING</a>
                            <ul class="sub pricing">
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."pricing/website"?> class="<?= ($path[2]=='website')?'active':'' ?>">Website</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."pricing/smm"?> class="<?= ($path[2]=='smm')?'active':'' ?>">Social Media Marketing</a></li>
                                <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."pricing/seo"?> class="<?= ($path[2]=='seo')?'active':'' ?>">Search Engine Optimization</a></li>
                            </ul>
                        </li>-->
                        <li class="nav-full-item parentNav">
                          <a class="mobileClass <?= ($path[1]=='about')?'active':'' ?>" target="_self"  title="About">ABOUT</a>
                            <ul class="sub about">
                            <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."about/company"?> class="<?= ($path[2]=='company')?'active':'' ?>">Company</a></li>
                            <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."about/management"?> class="<?= ($path[2]=='management')?'active':'' ?>">Management</a></li>
                            <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."about/team"?> class="<?= ($path[2]=='team')?'active':'' ?>">Team</a></li>
                            <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."about/life"?> class="<?= ($path[2]=='life')?'active':'' ?>">Life at Redot</a></li>
                            <li><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<a href=<?= $ini_array['path']."about/careers"?> class="<?= ($path[2]=='careers')?'active':'' ?>">Careers</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="<?= ($path[1]=='contact')?'active':'' ?>" target="_self"  href=<?= $ini_array['path']."contact"?> title="CONTACT">CONTACT</a></li></ul>
                </div>
            </div>
        </div>
    </nav>
    </div>
</header>