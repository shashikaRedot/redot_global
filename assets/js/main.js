// mobile nav push
$(function(){
    $('.mobile-menu').click(function(){
        $('html').toggleClass('menu-on');
    });
    $('.main-menu .link, .menu-body').hover(function(){
        $(this).parent().addClass('link-active');
    }, function(){
        $(this).parent().removeClass('link-active');
    });
    $('.link').click(function(t){
        if($(this).parent('.link-active').length > 0)
        {
            $(this).parent().removeClass('link-active');
        }else{
            $(this).parent().addClass('link-active');
        }
    });
});

$('body').click(function(e) {
    if($(e.target).parents().hasClass('wrapper home')){
        if($('html').hasClass('menu-on')){
            $('html').removeClass('menu-on');
        }
    }
 });


// scroll to location
$('.scroll-to').click(function(event){
  //event.preventDefault();
  $('html,body').animate({scrollTop:$(this.hash).offset().top-50}, 500);
});

//flip portfolio
$(document).ready(function(){
    $(".card").flip({
        trigger: 'hover'
	});
});	

//animate on scroll
AOS.init({
    offset: 200,
    duration: 600,
    easing: 'ease-in-sine',
    delay: 300,
    disable: 'mobile'
  });


//contact form submittion 
$("#submit-btn").click(function(event) {
    var _name = $("#inputname");
    var _subject = $("#inputsubject");
    var _message = $( "#inputmsg");
    var _email = $( "#inputemail");
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,10}\b$/i
    var _required = 'Required field cannot be empty.'


    //event.preventDefault();
    
    if(_name.val().trim() == ''){
        _name.addClass('form-error');
        _name.parent().children('#error').text(_required);
    }else{
        _name.removeClass('form-error');
        _name.parent().children('#error').text('');
    }

    if(_subject.val().trim() == ''){
        _subject.addClass('form-error');
        _subject.parent().children('#error').text(_required);
    } else {
        _subject.removeClass('form-error');
        _subject.parent().children('#error').text('');
    }

    if(_message.val().trim() ==''){
        _message.addClass('form-error');
        _message.parent().children('#error').text(_required);
    }else {
        _message.removeClass('form-error');
        _message.parent().children('#error').text('');
    }

    if( _email.val().trim() ==''){
        _email.addClass('form-error');
        _email.parent().children('#error').text(_required);
    }else if(!pattern.test(_email.val().trim())){
        _email.parent().children('#error').text('Please enter valid email address');
        _email.addClass('form-error');
    }
    else {
        _email.parent().children('#error').text('');
        _email.removeClass('form-error');
    }
    
    if($('.form-error').length > 0){
        return false;
    }else{
        if(grecaptcha.getResponse().length > 0){
            return true;
        }else{
            $('#captchaError').text('Prove your self as a human');
            return false;
        }
    }

})

if($( window ).width() > 880){
    particlesJS.load('particles', './config/particle.json', function() {
        // console.log('callback - particles.js config loaded');
    });
}

//recaptcha check
var recaptchaCheck = function(res){}


//full-screen-menu
$('.menu-toggle').click(function(){
    //alert();
    //('html,body').toggleClass('hiddenOverflow');
    $(this).toggleClass('open');
    $('#full-screen-menu').toggleClass('menuOpen');
    // $('.navbar-fixed-top').toggleClass('mainNavOpen');
    // if($('#navbar').hasClass('navOpen')){
    //         $('html,body').addClass('hiddenOverflow');
    //         $('#navbar .navWrap').getNiceScroll().show();
    // }else{
    //     $('html,body').removeClass('hiddenOverflow');
    //     $('#navbar .navWrap').getNiceScroll().hide();
    // }
    // if($(this).hasClass('open')){
    //         $('.sliderDesktop .ls-bottom-nav-wrapper').hide();
    // }else{
    //     $('.sliderDesktop .ls-bottom-nav-wrapper').show();
    // }
});


