<?php
$pageTitle = 'Sas | Portfolio | Redot Software Solutions';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="sas">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Background information about Redot Team</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Portfolio</li>
                        <li class="active">My SAS</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page" style="padding-bottom:0;" data-aos="fade-up">
            <div class="container">
                <h2 class="sub_page_header text-center title"><span>My SAS</span></h2>
                <p class="text-center">An ERP platform for a leading New Zealand sportswear manufacturing and marketing company.</p>
                <!-- <div class="portfolio-block">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 no-gutters">
                                <blockquote class="quote-box">
                                    <div class="quotation-mark"><i class="fa fa-quote-left fa-pull-left" aria-hidden="true"></i></div>
                                    <p class="quote-text">The entire design, development and implementation process with 4mation was extremely positive resulting in an excellent system that has delivered value to both our customers and SG Fleet. 4mation are a great team to work with and I look forward to our next collaboration.</p>
                                    <p class="quote-name">Chris Horton, Application Development Manager, SG Fleet</p>
                                </blockquote>
                        </div>
                    </div>
				</div> -->
			</div>
            <div class="container-fluid no-gutters">
                <div id="wideCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                    <li data-target="#wideCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#wideCarousel" data-slide-to="1"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <div class="item active">
                        <img src=<?= $ini_array['path']."assets/images/pub/mysas02.jpg"?> alt="">
                    </div>

                    <div class="item">
                        <img src=<?= $ini_array['path']."assets/images/pub/mysas01.jpg"?> alt="">
                    </div>

                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#wideCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#wideCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h2>What we did</h2>
                        <ul class="portfolio-list">
                            <li>Daily business activities have been transformed into system processes.</li>
                            <li>High security protocols for the system backend.</li>
                            <li>Special attention was given to potential system expansions from the blueprint itself.</li>
                            <li>Designed for agile requirement adoptions.</li>
                            <li>Big data handling.</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <h2>Technologies used</h2>
                        <ul class="portfolio-list">
                            <li>NodeJs</li>
                            <li>AngularJs</li>
                            <li>AWS Services</li>
                            <li>REST API</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid porfolio-wide-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h2>Case study</h2>
                            <p>SAS Sports is one of the most successful sportswear brands in New Zealand. Initially, SAS approached the Redot team in creating a far more advance, yet very user friendly real-time platform for their ordering process. Once it is completed, both the teams realized the potential of developing the project to a Full ERP solution. Redot team started the initial feasibility study in collaboration with the SAS team adding the first development to the system taking manufacturing in to the system. Gradually, Designing, accounting and finance were added to the system.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid porfolio-wide-dark-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h2>Where we are now?</h2>
                            <p>Team was able to identify the potential and in the process developing a fast and user friendly ERP solution for the company in a very short time period.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>   