<?php
$pageTitle = 'Effro | Portfolio | Redot Software Solutions';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="effro">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Background information about Redot Team</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Portfolio</li>
                        <li class="active">Effro</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page" style="padding-bottom:0;" data-aos="fade-up">
            <div class="container">
                <h2 class="sub_page_header text-center title"><span>Effro</span></h2>
                <p class="text-center">To create an air bnb like market place for the talent market.</p>
                <!-- <div class="portfolio-block">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 no-gutters">
                                <blockquote class="quote-box">
                                    <div class="quotation-mark"><i class="fa fa-quote-left fa-pull-left" aria-hidden="true"></i></div>
                                    <p class="quote-text">The entire design, development and implementation process with 4mation was extremely positive resulting in an excellent system that has delivered value to both our customers and SG Fleet. 4mation are a great team to work with and I look forward to our next collaboration.</p>
                                    <p class="quote-name">Chris Horton, Application Development Manager, SG Fleet</p>
                                </blockquote>
                        </div>
                    </div>
				</div> -->
			</div>
            <div class="container-fluid no-gutters">
                <div id="wideCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                    <li data-target="#wideCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#wideCarousel" data-slide-to="1"></li>
                    <li data-target="#wideCarousel" data-slide-to="2"></li>
                    <li data-target="#wideCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <div class="item active">
                        <img src=<?= $ini_array['path']."assets/images/pub/effro-01.png"?> alt="">
                    </div>

                    <div class="item">
                        <img src=<?= $ini_array['path']."assets/images/pub/effro-02.png"?> alt="">
                    </div>
                    
                    <div class="item">
                        <img src=<?= $ini_array['path']."assets/images/pub/effro-03.png"?> alt="">
                    </div>

                    <div class="item">
                        <img src=<?= $ini_array['path']."assets/images/pub/effro-04.png"?> alt="">
                    </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#wideCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#wideCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h2>What we did</h2>
                        <ul class="portfolio-list">
                            <li>Mobile first responsive UX design</li>
                            <li>Development of a large-scale API-driven single page application</li>
                            <li>Performance optimization</li>
                            <li>Real time messaging</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <h2>Technologies used</h2>
                        <ul class="portfolio-list">
                            <li>NodeJs</li>
                            <li>AngularJs</li>
                            <li>AWS</li>
                            <li>CDN</li>
                            <li>REST API</li>
                            <li>Circle CI</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid porfolio-wide-bg">
                <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h2>Case study</h2>
                        <p>Redot team was given the challenge of creating an online market space for talent hiring in 2015 March. After an intense research done by the team in collaboration with the Singapore team few of the crucial factors were identified in the industry.</p>
                        <ul class="portfolio-list">
                            <li>Online market places are growing around the globe rapidly in the service sector.</li>
                            <li>Blue ocean for an online talent market place.</li>
                            <li>Opportunity of using the latest technologies such as NodeJs, AngularJs</li>
                        </ul>
                        <p>With the findings of the research the team presented an outstanding digital plan for the way forward of Effro, empowered by latest technologies with main objective of developing an Air BnB type of a digital platform for Effro. Along with the approvals the team started building an effective platform where both the talent and the talent hunter can easily communicate with each other and the needs will be met.</p>
                    </div>
                </div>
                </div>
            </div>
            <div class="container-fluid porfolio-wide-dark-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h2>Where we are now?</h2>
                            <p>Key success of this project is creating an online talent market place where the venders can register their portfolio and customers can easily pick according to their needs with an effective multi-end communication.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>   