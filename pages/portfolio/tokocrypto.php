<?php
$pageTitle = 'Tokocrypto | Portfolio | Redot Software Solutions';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="tokocrypto">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Background information about Redot Team</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Portfolio</li>
                        <li class="active">Tokocrypto</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page" style="padding-bottom:0;" data-aos="fade-up">
            <div class="container">
                <h2 class="sub_page_header text-center title"><span>Tokocrypto</span></h2>
                <p class="text-center">To create a Crypto currency trading platform mainly targeting Indonesia.</p>
                <!-- <div class="portfolio-block">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 no-gutters">
                                <blockquote class="quote-box">
                                    <div class="quotation-mark"><i class="fa fa-quote-left fa-pull-left" aria-hidden="true"></i></div>
                                    <p class="quote-text">The entire design, development and implementation process with 4mation was extremely positive resulting in an excellent system that has delivered value to both our customers and SG Fleet. 4mation are a great team to work with and I look forward to our next collaboration.</p>
                                    <p class="quote-name">Chris Horton, Application Development Manager, SG Fleet</p>
                                </blockquote>
                        </div>
                    </div>
				</div> -->
			</div>
            <div class="container-fluid no-gutters">
                <div id="wideCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                    <li data-target="#wideCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#wideCarousel" data-slide-to="1"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <div class="item active">
                        <img src=<?= $ini_array['path']."assets/images/pub/tokocrypto01.jpg"?> alt="">
                    </div>

                    <div class="item">
                        <img src=<?= $ini_array['path']."assets/images/pub/tokocrypto02.jpg"?> alt="">
                    </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#wideCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#wideCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h2>What we did</h2>
                        <p>Create a platform for,</p>
                        <ul class="portfolio-list">
                            <li>Instant Rupiah deposit. Instant purchase of digital assets. Instant gratification.</li>
                            <li>Separation of funds. The wallet is safe. Strong procedure.</li>
                            <li>Digital currency wallet that securely stores your digital assets</li>
                            <li>Digital asset wallet is supported by Market Leader Bitgo. Most of the funds are kept securely in an offline wallet.</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <h2>Technologies used</h2>
                        <ul class="portfolio-list">
                            <li>NodeJs</li>
                            <li>AWS</li>
                            <li>CDN</li>
                            <li>REST API</li>
                            <li>ReactJS</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid porfolio-wide-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h2>Case study</h2>
                            <p>Redot team was briefed by Tokocrypto on developing a Crypto currency trading platform mainly targeting the Indonesian nationals. As specially requested by the client, platform was built with the latest technologies such as ReactJs and use of the most prominent security protocols available in the world. Platform was built following four basic principles as requested by the client,</p>
                            <ul class="portfolio-list">
                                <li>Easy – Register and start investing</li>
                                <li>Simple – Built by investors for the investors</li>
                                <li>Instant – Instant rupee deposit, purchase of digital assets and gratification</li>
                                <li>Secure – Sophisticated security protocols.</li>
                            </ul>
                            <p>Also Tokocrypto is designed to facilitate selling and purchasing of all kind of well-known digital assets such as Bitcoin, Ethereum, Ripple, Litecoin, Neo, Cardano and Swipe. Network is linked with the indonisian bank system and facilitate instant conversion of digital assets into Indonesian rupiah. Asset store is shenduled to be launched to the public by the 8th of April 2018.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid porfolio-wide-dark-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h2>Where we are now?</h2>
                            <p>Team could successfully develop Tokocrypto, aims to be the Southeast Asia’s leading exchange for digital assets by providing customers with an easy, simple, instant, and secure platform to transact with confidence.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>   