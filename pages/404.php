<?php include('../_partials/header.php'); ?>
<?php include('../_partials/menu.php'); ?>
<div class="wrapper home">
    <img src="/assets/images/pages/404.png" alt="404">
    <div class="no-page" style="text-align:center; margin-bottom:100px; padding:15px;">
        <p>The page you are looking for doesn't exist or another error occurred.</p>
        <a href="/">Go back</a>
    </div>
</div>
<?php include('../_partials/footer.php'); ?>  