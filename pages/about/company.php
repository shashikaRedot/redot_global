<?php
$pageTitle = 'Company | About | Redot Software Solutions';
$pageMetaDescription = 'In the past 7 years Redot has been evolving rapidly as a leading software company based in Sri Lanka. We provide a wide range of IT services and always believe that the client’s perception is the key. To get the best quality services at the most competitive prices, contact us.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="about redot">
            <div class="container">
                <h2 class="title">About <span>Us</span></h2>
                <hr>
                <p>Background information about Redot Team</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>About</li>
                        <li class="active">Company</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <div class="col-md-12 sub" data-aos="fade">
                    <h1 class="sub_page_header text-center title"><span class="brand-color"><span class="text-blck" style="color:#000;">RE</span>DOT,</span> your one-stop IT and Digital Media Solution.</h1>
                    <p class="text-center">REDOT is an IT and Digital Media Solution Company founded in the year2012 focusing on providing highly scalable e-business solutions to our clientswith innovative approaches and advanced methodologies.</p>
                    <img src=<?= $ini_array['path']."assets/images/pages/about_inner.png"?> alt="Redot are a software company in Sri Lanka providing the IT solutions">
                </div>
                <div class="col-md-12 sub"  data-aos="fade">
                    <div class="col-md-6">
                        <h2>With a dedicated team of highly skilled and specialized personnel we provide you,</h2>
                        <ul>
                            <li>Digital Strategy</li>
                            <li>Web Design and Development</li>
                            <li>eCommerce Development</li>
                            <li>Mobile Apps</li>
                            <li>System Integration</li>
                            <li>Managed Cloud Services</li>
                            <li>Digital Marketing</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <img src=<?= $ini_array['path']."assets/images/pages/about_inner_1.png"?> alt="Redot team is always at your service ensuring you are satisfied with our services">
                    </div>
                </div>
                <div class="col-md-12 sub"  data-aos="fade">
                    <div class="col-md-6">
                        <img src=<?= $ini_array['path']."assets/images/pages/about_inner_2.png"?> alt="Redot has an experienced group of developers who has worked with the international">
                    </div>
                    <div class="col-md-6">
                        <h2>Never let you down</h2>
                        <p>Our services even have seekers in Singapore, US, New Zealand and our portfolio speaks for itself. For each project, we provide an individual, a multiplatform team of experienced and best talented e-business specialists with substantial backgrounds in all services.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  