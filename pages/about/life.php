<?php
$pageTitle = 'Life at Redot | About | Redot Software Solutions';
$pageMetaDescription = 'Life at Redot is life at home. We always ensure that we employ the right personnel for the right jobs.  Let the job be complicated or tough, we believe in overcoming that by working as a team and living as a family.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="life at redot">
            <div class="container">
                <h2 class="title">Life</h2>
                <hr>
                <p>Our family is our strength</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>About</li>
                        <li class="active">Life</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
            <h2 class="sub_page_header text-center title"><span>Our family</span> during the day time!</h2>
            <p class="text-center">We like to challenge the ideas of the norm to create unique, solution based websites in order to maximise your online presence.</p>
            </div>
            <div class="container-fluid redot-life">
                <div class="row life-block" data-aos="fade">
                    <div class="col-md-6 no-gutters">
                        <div class="life-block-media">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/life-redot.jpg"?> alt="">
                        </div>
                    </div>
                    <div class="col-md-6 no-gutters">
                        <div class="life-block-text">
                            <h2>What makes us more than another office</h2>
                            <p>Redot Global puts an extraordinary amount of effort when interviewing candidates to insure that all employees fit the “culture”.
                            As well, there is a ton of emphasis placed on Leadership, Trust, Integrity, Teamwork, Synergy and “giving back”.
                            We lead with our hearts but also with an urgency to “Get Things Done”. We are accountable and respectful all at the same time.</p>
                        </div>
                    </div>
                </div>
                <div class="row life-block" data-aos="fade">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters">
                        <div class="life-block-media">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/life-redot-2.jpg"?> alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters">
                            <div class="life-block-text">
                                <h2>We are our second family</h2>
                                <p>Enthusiastic and encouraging management that aligns employee’s individual goals with the company’s overall direction is another factor that we consider very important to make a company a great place to work.
                                Redot Global provides an environment that encourages open lines of communication, visibility across the organization and most importantly a place where you can pursue one’s individual interests.
                                Fostering an environment that encourages a common goal of doing the right thing for the customer and making them successful.</p>
                            </div>
                    </div>
                 </div>
                <div class="row life-block" data-aos="fade">
                    <div class="col-md-6 no-gutters">
                        <div class="life-block-media">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/life-redot-3.jpg"?> alt="">
                        </div>
                    </div>
                    <div class="col-md-6 no-gutters">
                        <div class="life-block-text">
                            <h2>No one gets left behind</h2>
                            <p>Culture plays a vital role in the success of a company. At Redot, our culture creates commonalities between employees and brings them closer to work as a single team.
                            Since management sits in the open with no doors or locks, it makes them available to the needs of their employees all of the time.</p>
                        </div>
                    </div>
                </div>
                <div class="row life-block" data-aos="fade">
                    <div class="col-lg-7 col-lg-push-5 col-md-7 col-md-push-5 col-sm-12 no-gutters">
                        <div class="life-block-text-ex">
                            <h2>We never get bored</h2>
                            <p>We have never felt any hesitation to go and engage with our peers, membersfrom other teams or management when in need, and the culture at Redotencourages this collaboration. This open culture is effective and efficient.</p>
                        </div>
                        <div class="life-block-media">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/life-redot-4.jpg"?> alt="">
                        </div>
                    </div>
                    <div class="col-lg-5 col-lg-pull-7 col-md-5 col-md-pull-7 col-sm-12 no-gutters">
                            <div class="life-block-text">
                                <h2>Why get stressed</h2>
                                <p>Redot Global not only encourages us to work hard but also enjoy the company of those we work with. Gaming competitions, a beer bash, a junk food night are a few perfect ways to wind down at the end of the week and have a good time meeting fellow employees.
                                It brings everyone together where they can share their thoughts and get ready for the week ahead.</p>
                            </div>
                    </div>
                </div>        
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  