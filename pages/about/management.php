<?php
$pageTitle = 'Management | About | Redot Software Solutions';
$pageMetaDescription = 'We always believe in our expert crew and we were never let down by them. Redot Management goes by the motto “ Leadership is not a position, but an action”!';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot management">
            <div class="container">
                <h2 class="title">Management</h2>
                <hr>
                <p>Background information about Redot Team</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>About</li>
                        <li class="active">Management</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page"  data-aos="fade-up">
            <div class="container">
            <h2 class="sub_page_header text-center title">Meet the <span>Management</span></h2>
            <p class="text-center">We believe in leadership as an action not as a position.</p>
                <!--<div class="row members">
                    <div class="col-md-6 member">
                        <div class="member-photo-frame">
                            <img src="../assets/images/pages/about/ceo.jpg">
                        </div>
                        <h3 class="text-brand">Joemon Mathew</h3>
                        <p class="text-brand">CEO</p>
                        <p>World around us keeps changing and keeps getting challenging. In Redot our team looks ahead, Analyze the trends and technologies to help our clients and us to be ready for tomorrow today. With a team compiled with Passion dedication and Excellent skill sets. Our Vison is our guide to keep us and our clients winning.</p>
                    </div>
                    <div class="col-md-6 member">
                        <div class="member-photo-frame">
                            <img src="../assets/images/pages/about/blank.jpg">
                        </div>
                        <h3 class="text-brand">Suneth Silva</h3>
                        <p class="text-brand">Co-Founder</p>
                        <p>Redot has a mission that’s enduring. Our products and services would follow our values. We are here to provided solutions crafted for our client needs. Trust and Quality is the foundation that we began the company with. With every client, customer or team member added. We make sure to uphold our values.</p>
                    </div>
				</div>-->
				<div class="row members">
                    <div class="col-md-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/md.jpg"?> alt="kavinda silva">
                        </div>
                        <h3 class="text-brand">Kavinda Silva</h3>
                        <p class="text-brand">Managing Director</p>
                        <p>We thrive to provide stellar commitment to productivity. Bring to the world a portfolio of quality accomplishments and happy clients. We make sure to be a happy work place with inspired individuals so they can bring their best and quality output working towards our goal of being an effective and fast-growing organization. With opportunities for Research and development along with knowledge sharing to bring out the best essence in current technologies.</p>
                    </div>
					<div class="col-md-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/dakshina-udayanga.jpg"?> alt="dakshina udayanga">
                        </div>
                        <h3 class="text-brand">Dakshina Udayanga</h3>
                        <p class="text-brand">Executive Director/Tech Lead</p>
                        <p>We are nurturing a team responsive to change and who can act with urgency all while keeping up the efficiency. Our Business solutions cater to IT solutionsand marketing solutions. We focus on our customer needs, market trends and at the same time inspire creativity passion and optimism as a development team.</p>
                    </div>
                </div>        
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  