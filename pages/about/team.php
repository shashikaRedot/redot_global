<?php
$pageTitle = 'Team | About | Redot Software Solutions';
$pageMetaDescription = 'The Redot expert team who are more of a family. Meet us here.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot developers">
            <div class="container">
                <h2 class="title">Team</h2>
                <hr>
                <p>Background information about Redot Team</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>About</li>
                        <li class="active">Team</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page"  data-aos="fade-up">
            <div class="container">
                <h2 class="sub_page_header text-center title">Meet the <span>Team</span></h2>
                <p class="text-center">We are a one big family who work passionately assisting each other towards their personal and professional growth.</p>
                <!-- <div class="text-center">
                    <img src=<?= $ini_array['path']."assets/images/pages/about/team/team.png"?>>
                </div> -->
                <div class="team members">
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/thushara-software-engineer.jpg"?> alt="thushara ravishanka">
                        </div>
                        <h3 class="text-brand">Thushara Ravishanka</h3>
                        <p>Software Engineer</p>
                    </div>
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/ranidu-software-engineer.jpg"?> alt="ranidu nadeesha">
                        </div>
                        <h3 class="text-brand">Ranidu Nadeesha</h3>
                        <p>Software Engineer</p>
                    </div>
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/nadun-software-engineer.jpg"?> alt="nadun lanka">
                        </div>
                        <h3 class="text-brand">Nadun Lanka</h3>
                        <p>Software Engineer</p>
                    </div>
                </div>
                <div class="team members">
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/lakshan-software-engineer.jpg"?> alt="lakshan kasun">
                        </div>
                        <h3 class="text-brand">Lakshan Kasun</h3>
                        <p>Software Engineer</p>
                    </div>
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/shiyan-software-engineer.jpg"?> alt="shiyan sardeen">
                        </div>
                        <h3 class="text-brand">Shiyan Sardeen</h3>
                        <p>Software Engineer</p>
                    </div>
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/oshada-software-engineer.jpg"?> alt="oshadha dushan">
                        </div>
                        <h3 class="text-brand">Oshadha Dushan</h3>
                        <p>Software Engineer</p>
                    </div>
                </div>
                <div class="team members">
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/nuwan-software-engineer.jpg"?> alt="nuwan srinath">
                        </div>
                        <h3 class="text-brand">Nuwan Srinath</h3>
                        <p>Software Engineer</p>
                    </div>
 
                    <div class="col-md-4 col-sm-6 member">
                        <div class="member-photo-frame">
                            <img src=<?= $ini_array['path']."assets/images/pages/about/shashika-ui-engineer.jpg"?> alt="shashika priyanakra">
                        </div>
                        <h3 class="text-brand">Shashika Priyankara</h3>
                        <p>UI Engineer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  