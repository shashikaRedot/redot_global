<?php
$pageTitle = 'Careers | About | Redot Software Solutions';
$pageMetaDescription = 'We like to challenge the ideas of the norm to create unique, solution based websites in order to maximise your online presence';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot careers">
            <div class="container">
                <h2 class="title">Careers</h2>
                <hr>
                <p>Our family is our strength</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>About</li>
                        <li class="active">Careers</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page"  data-aos="fade-up">
            <div class="container">
                <h2 class="sub_page_header text-center title"><span>We are</span> looking for</h2>
                <p class="text-center">We like to challenge the ideas of the norm to create unique, solution based websites in order to maximise your online presence.</p>
            </div>
                <div class="container-fluid main-block">
                    <div class="col-md-12 no-gutters">
                        <div class="join-us-block-text col-md-6 col-md-push-6 col-sm-6 col-sm-push-6">
                            <div class="job-block">
                                <h3>Social Media Marketing ( Junior Executive)</h3>
                                <span class="exp-block">1+ Yrs Exp</span>
                                <!-- <p><strong>
                                Post updates, news and other promotional activities on social media channels in a timely manner, using appropriate content<br/>
                                Drive social media marketing strategies and promotions over appropriate channels.<br/>
                                Develop a strategic and ongoing editorial content calendar by creating engaging content, and execute the same over respective social media channels<br/>
                                Monitor conversions taking place across all social media channels on a real time basis, and take action as needed to engage with the community<br/>
                                </strong></p> -->
                                <ul>
                                    <li>Should be age below 35</li>
                                    <li>Should have excellent communication skills in English (Written and Oral)</li>
                                    <li>Savvy with Facebook, Twitter, Instagram, LinkedIn, YouTube, Pinterest and other social media</li>
                                    <li>Should be able to handle multiple social media platforms</li>
                                    <li>Should have can-do attitude, creating engaging content, and have a creative and have a creative and innovative way of think</li>
                                    <li>Skills in graphics designing will be an added advantage</li>
                                    <li>Salary negotiable</li>
                                </ul>
                            </div>
                            <div class="job-block">
                                <h3>Software Engineer - PHP</h3>
                                <span class="exp-block">2 -3 Yrs Exp</span>
                                <!-- <p><strong>
                                WPF/Silverlight, C#, Nhibernate, Sql server, WCF, REST, GIT.
                                Experience with javascript development will be an advantage. </strong></p> -->
                                <ul>
                                    <li>Experience in PHP and WordPress</li>
                                    <li>Expert knowledge of web technologies. (HTML, CSS, Bootstrap and jQuery)</li>
                                    <li>Capability of customizing WordPress themes and plugins</li>
                                    <li>Experienced in Laravel Framework/ OpenCart/ Prestashop  will be an added advantage</li>
                                    <li>Knowledge in AngularJS is a Plus</li>
                                </ul>
                            </div>
                            <div class="job-block">
                                <h3>Software Engineer - JavaScript</h3>
                                <span class="exp-block">2+ Yrs Exp</span>
                                <!-- <p><strong>
                                WPF/Silverlight, C#, Nhibernate, Sql server, WCF, REST, GIT.
                                Experience with javascript development will be an advantage. </strong></p> -->
                                <ul>
                                    <li>Bsc in Computer Science/ Engineering or a related field</li>
                                    <li>Strong understanding in User Experience and User Interface design Proficiency in NodeJS, AngularJS</li>
                                    <li>Knowledge in ReactJS and AWS Services would be cool!</li>
                                    <li>Well experienced in Relational Databases</li>
                                    <li>Excellent in communication</li>
                                    <li>Proficiency in AJAX, JASON and related web/ interface technologies</li>
                                </ul>
                            </div>
                            <div class="job-block last">
                                <h3>Administrative Assistant</h3>
                                <span class="exp-block">1 Yr Exp</span>
                                <!-- <p><strong>
                                WPF/Silverlight, C#, Nhibernate, Sql server, WCF, REST, GIT.
                                Experience with javascript development will be an advantage. </strong></p> -->
                                <ul>
                                    <li>Successful completion of G.C.E Ordinary Level</li>
                                    <li>Should be able to handle and maintain office correspondence and compile data and reports in an accurate and efficient manner</li>
                                    <li>Average computer literacy</li>
                                    <li>Comprehensive working knowledge of MS office package</li>
                                    <li>Should have excellent communication skills in English (Written and Oral)</li>
                                    <li>Excellent analytical and numerical skills with a meticulous work ethics</li>
                                    <li>Excellent communication and interpersonal abilities</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                        <div class="row join-us-block"></div>
                    <div class="col-md-12 no-gutters">
                        <div class="join-us-block-contact col-md-6 col-md-push-6 col-sm-6 col-sm-push-6">
                            <div class="join-us-block-details">
                            <h3>Good verbal communication Experience with Agile Process (Scrum) Degree in Software development or equivalent</h3>
                            <p>You should be passionate, initiative and good verbal and written communication skills.<br/>
                            Drop your resume at <a href="mailto:carrers@redot.global">careers@redot.global</a><br/>
                            Subject should be [Job Title] Your Name<br/>
                            Eg. [Tech Lead] Sampath Fernando</p>
                            </div>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  