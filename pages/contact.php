<?php
$pageTitle = 'Contact Us | Redot Software Solutions';
$pageMetaDescription = 'Feel free to contact us for any inquiries regarding business or our company policies. Call us, T: +94 112 612 627 and Email at E: info@redot.global.';
include('../_partials/header.php'); ?>
<?php include('../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="contact redot">
            <div class="container">
                <h2 class="title">Contact <span>Us</span></h2>
                <hr>
                <p>Background information about Redot Team</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Pages</li>
                        <li class="active">Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="contact-us page">
			<div class="container-fluid">
				<div class="row">			
					<div class="col-md-8 map-cav" style="padding-left:0;">
						<div id="map" class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.546850213644!2d79.88021731412516!3d6.824833295067497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae25ada64897293%3A0x3c5c96ac76ff2a37!2s3+Sri+Jinarathana+Rd%2C+Dehiwala-Mount+Lavinia!5e0!3m2!1sen!2slk!4v1545708492210" width="100%" height="900" frameborder="0" style="border:0" allowfullscreen></iframe>						</div>
					</div>
					<div class="col-md-4" data-aos="fade">
						<div class="contact">
							<h2>Get in touch!</h2>
							<p class="social">
								<span>
									<a href="https://www.facebook.com/redot.global/" target="_blank"><i class="fab fa-facebook"></i></a>
									<a href="https://youtu.be/sw4gUo_i9eA" target="_blank"><i class="fab fa-youtube-square"></i></a>
									<a href="https://www.instagram.com/redot.global" target="_blank"><i class="fab fa-instagram"></i></a>
								</span>
							</p>
							<p class="contact-details">
								<strong>T:</strong> <a class="tel" href="tel:+94112612627"><strong>+94 112 612 627</strong></a><br/>
								No 03,<br/> 
								Sri Jinarathana Mawatha,<br/> 
								Rathmalana,<br/> 
								Sri Lanka.<br/>
								<strong>E: <a href="mailto:info@redot.global">info@redot.global</a></strong>
							</p>
							<h2>Have a question?</h2> 
							<?= ($_GET["status"]=="done")?"<h5 class='mail-success'>We'll get back to you soon.</h5>":"";?>
							<?= ($_GET["status"]=="error")?"<h5 class='mail-error'>Error occurred while sending mail.</h5>":"";?>   
							<form class="form" action=<?= $ini_array['path']."mail.php"?> method="POST">
								<div class="form-group" id="contact-redot">
									<label for="inputname">Your Name</label>
									<input type="text" class="form-control" id="inputname" name="name" placeholder="Your Name">
									<div id="error" class="error"></div>
								</div>
								<div class="form-group">
									<label for="inputemail">Your Email</label>
									<input type="email" class="form-control" id="inputemail" name="email" placeholder="Your Email">
									<div id="error" class="error"></div>
								</div>
								<div class="form-group">
									<label for="inputsubject">Subject</label>
									<input type="text" class="form-control" id="inputsubject" name="subject" placeholder="Your Subject">
									<div id="error" class="error"></div>
								</div>
								<div class="form-group">
									<label for="inputmsg">Message</label>
									<textarea class="form-control" id="inputmsg" name="message" placeholder="Your Subject" rows="5"></textarea>
									<div id="error" class="error"></div>
								</div>
								<div class="form-group">
									<div class="g-recaptcha" data-callback="recaptchaCheck" data-sitekey="6Lf7mG8UAAAAAMqne7HEeh9mWNrjhhXZMCCQuuc1"></div>
									<div id="captchaError" class="error"></div>
								</div>    
								<button type="submit" class="btn btn-default" id="submit-btn">Submit</button>
							</form>
						</div>
					</div>
				</div>			
			</div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    
    <?php include('../_partials/footer.php'); ?>          