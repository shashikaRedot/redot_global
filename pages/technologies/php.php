<?php
$pageTitle = 'PHP Development | Technologies | Redot Software Solutions';
$pageMetaDescription = 'PHP is still among the most prominent back end coding technologies. We make use of Laravel, Symfony, Zend and etc. frameworks and will help to meet your customized application requirements.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header technology tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="php development">
            <div class="container">
                <h2 class="title">PHP <span>Development</span></h2>
                <hr>
                <p>PHP is a server- side scripting language primarily used in web development.  PHP as a web development option is secure, fast and reliable. Take a look at our PHP capabilities.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Technologies</li>
                        <li class="active">PHP Development</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="technology page">
			<div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/laravel_logo.png"?> alt="Redot developers have mastered the Laravel Development">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Laravel Development</h2>
                        <p>Laravel is an open source PHP framework mainly used for rapid web development. It also provides business and structure logic for businesses.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/symfony_logo.png"?> alt="Symfony is one of the leading PHP frameworks in the industry today">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Symfony Development</h2>
                        <p>Symfony is an open source PHP framework with MVC architecture used to build complex web applications with high performance.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/zend_logo.png"?> alt="Zend is also among the PHP frameworks that Redot use in web development">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Zend Development</h2>
                        <p>Zend is an open source, object oriented PHP Framework consisting multiple professional PHP packages</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/php_logo.png"?> alt="Redot developers are capable of Custom PHP development according to your requirements">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Custom Development</h2>
                        <p>If you want customized PHP solutions, reach out Redot. Our developers are having a wide range of skills led by PHP development.</p>
                    </div>
                </div>
            </div>
			<div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/codeigniter_logo.png"?> alt="Codeignitor framework is also used by us Redot in PHP development">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Codeigniter Development</h2>
                        <p>CodeIgniter is used for building dynamic websites with PHP. It is one of the mostly used frameworks in open source development.</p>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
    <div class="footer-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="title">Ready to get started?</h2>    
                </div>
                <div class="col-md-8">
                    <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                    <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../../_partials/footer.php'); ?>  