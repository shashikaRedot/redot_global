<?php
$pageTitle = 'Mobile Development | Technologies | Redot Software Solutions';
$pageMetaDescription = 'When you want to make your brand unique, what better than creating a mobile app? Our team is fluent in native IOS development, android development and react development as well to customize your app according to your needs.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header technology tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="mobile development">
            <div class="container">
                <h2 class="title">Mobile</span></h2>
                <hr>
                <p>Connectivity is very important when  you are running a business. Having your business available on mobile allows you to carry your day today business activities with ease.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Technologies</li>
                        <li class="active">Mobile</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="technology page">
			<div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/ios_native_logo.png"?> alt="Redot are good at IOS native development">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>IOS Native Development</h2>
                        <p>It is important to build a native IOS app since it is the best way to take advantages of Iphone functionalities including GPS, Camera and etc.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/android_native_logo.png"?> alt="Redot are providing Android Native Development services">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Android Native Development</h2>
                        <p>Native Android apps are specific to Android, which presents the following advantages:<br/>Native android apps are specific to Android influencing the speed and efficiency and the ability to interact with numerous phone functions.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/react_logo.png"?> alt="Redot does React Native Development as well">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>React Native Development</h2>
                        <p>React Native is a framework that allows web developers to create mobile applications utilizing their  JavaScript knowledge. It is used for faster mobile development.</p>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
    <div class="footer-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="title">Ready to get started?</h2>    
                </div>
                <div class="col-md-8">
                    <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                    <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../../_partials/footer.php'); ?>  