<?php
$pageTitle = '.Net Development | Technologies | Redot Software Solutions';
$pageMetaDescription = '.Net is a framework that can be used to develop a wide range of applications from web to mobile to Windows-based applications. Our engineers are fluent in the .Net platforms including .Net Core .Net MVC platforms and etc.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header technology tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt=".net development">
            <div class="container">
                <h2 class="title">.Net Development</span></h2>
                <hr>
                <p>.Net is a framework used to develop a wide range of applications which could be web, mobile, windows- based etc.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Technologies</li>
                        <li class="active">.Net Development</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="technology page">
			<div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/asp_asp.net_core_logo.png"?> alt="Redot can use Microsoft asp.net core for .Net Development">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Microsoft asp.net core</h2>
                        <p>ASP.NET Core is a cross-platform, high-performance, open-source framework for building modern, cloud-based, Internet-connected applications.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/asp_asp.net_mvc_logo.png"?> alt="Redot uses Microsoft asp.net MVC for .Net Development">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Microsoft asp.net mvc</h2>
                        <p>ASP.NET MVC framework is a lightweight, highly testable presentation framework that allows to develop robust web applications to cater your requirements.</p>
                    </div>
                </div>
            </div>
            <!--<div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/azure_logo.png"?> alt="azure logo">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Azure</h2>
                        <p>Microsoft Azure is a cloud computing service created by Microsoft for building, testing, deploying, and managing applications and services through a global network of Microsoft-managed data centers.</p>
                    </div>
                </div>
            </div>-->
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/microsoft_universal_platforms_logo.png"?> alt="microsoft universal platforms">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Microsoft Universal Windows Platform</h2>
                        <p>Universal Windows Platform (UWP), which provides a common app platform on every device (PC, tablet, Xbox, HoloLens, Surface Hub, IoT) that runs Windows 10.</p>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
    <div class="footer-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="title">Ready to get started?</h2>    
                </div>
                <div class="col-md-8">
                    <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                    <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../../_partials/footer.php'); ?>  