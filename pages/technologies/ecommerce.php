<?php
$pageTitle = 'Ecommerce | Technologies | Redot Software Solutions';
$pageMetaDescription = 'Your Ecommerce requirement could be B2B, B2C, C2C or C2C. We are capable of giving you the best solutions in a real quick time utilizing platforms including Shopify, Opencart, Magento, Prestashop etc.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header technology tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="ecommerce development">
            <div class="container">
                <h2 class="title">Ecommerce</span></h2>
                <hr>
                <p>Simply put, ecommerce refers to commercial transactions conducted online. This means that whenever you buy and sell something using the Internet.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Technologies</li>
                        <li class="active">Ecommerce</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="technology page">
			<div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/magento_logo.png"?> alt="Redot developers are capable of using Magento platform">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Magento</h2>
                        <p>Magento is an open-source e-commerce platform written in PHP. The softwarewas originally developed by Varien, Inc., a US private company headquarteredin Culver City, California, with assistance from volunteers.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/prestashop_logo.png"?> alt="Prestashop platform is used by Redot">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>PrestaShop</h2>
                        <p>PrestaShop is a freemium, open source e-commerce solution. The software is published under the Open Software License. It is written in the PHP programming language with support for the MySQL database management system.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/opencart_logo.png"?> alt="OpenCart is a PHP based online store management system">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>OpenCart</h2>
                        <p>OpenCart is an online store management system. It is PHP-based, using a MySQL database and HTML components. Support is provided for different languages and currencies. It is freely available under the GNU General Public License.</p> 
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/shopify_logo.png"?> alt="Shopify is a user friendly Ecommerce platform that Redot can use according to your requirements">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Shopify</h2>
                        <p>Shopify is a Canadian e-commerce company headquartered in Ottawa, Ontario. It is also the name of its proprietary e-commerce platform for online stores and retail point-of-sale systems.</p>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
    <div class="footer-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="title">Ready to get started?</h2>    
                </div>
                <div class="col-md-8">
                    <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                    <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../../_partials/footer.php'); ?>  