<?php
$pageTitle = 'Content Management Systems | Technologies | Redot Software Solutions';
$pageMetaDescription = 'User friendly back end will make it easier for you to manage your website content. Redot is capable of using globally recognized platforms such as Wordpress, Drupal, Joomla etc. according to your requirement.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header technology tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="content management systems">
            <div class="container">
                <h2 class="title">CMS</span></h2>
                <hr>
                <p>Create an online presence for your business or a personal website with a customised content management system which puts you in control.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Technologies</li>
                        <li class="active">CMS</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="technology page">
			<div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/wordpress_logo.png"?> alt="Redot are working with Wordpress">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>WordPress</h2>
                        <p>WordPress is a free and open-source content management system based on PHP and MySQL. Features include a plugin architecture and a template system.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <img src=<?= $ini_array['path']."assets/images/pages/technologies/drupal_logo.png"?> alt="Redot are using Drupal CMS as well">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Drupal</h2>
                        <p>Drupal is a free and open source content-management framework written in PHP and distributed under the GNU General Public License.</p>
                    </div>
                </div>
            </div>
            <div class="sub">
                <div class="container">
                    <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                        <img src=<?= $ini_array['path']."assets/images/pages/technologies/joomla_logo.png"?> alt="Redot create Joomla based CMS as well">
                    </div>
                    <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                        <h2>Joomla</h2>
                        <p>Joomla is a free and open-source content management system for publishingweb content. It is built on a model–view–controller web application framework.</p>  
                    </div>
                </div>
            </div>
        </div>        
    </div>    
    <div class="footer-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="title">Ready to get started?</h2>    
                </div>
                <div class="col-md-8">
                    <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                    <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../../_partials/footer.php'); ?>  