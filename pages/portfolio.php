<?php
$pageTitle = 'Portfolio | Redot Software Solutions';
$pageMetaDescription = 'Our portfolio depict you how we contributed to our clients businesses, leading them to have an efficient, user friendly and a cost effective workflow.';
include('../_partials/header.php'); ?>
<?php include('../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?>>
            <div class="container">
                <h2 class="title">Portfolio</h2>
                <hr>
                <p>Redot is active since 2012 and we have been catering online solutions all across the world as a leading Sri Lankan Software Company.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Pages</li>
                        <li class="active">Portfolio</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="portfolios page">
            <div class="container"data-aos="fade-up">
                <h2 class="sub_page_header text-center title">Customer <span>Stories</span></h2>
                <div class="col-md-4 portfolio effro">
                    <div class="portfolio_header">
                        <span class="portfolio_heading">Effro</span>
                        <h3 class="portfolio_subheading">Hosting an event at your fingertips. Effro provides you one stop platform for your event resources!</h3>
                    </div>
                    <div class="portfolio_media"></div>
                    <div class="portfolio_links">
                        <a class="portfolio_link" href=<?= $ini_array['path']."portfolio/effro"?>>
                            <span>Read story</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-288 379 35 35"><path d="M-260 386.5h-21c-.8 0-1.5.7-1.5 1.6s.7 1.6 1.5 1.6h21.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6zm0 8.4h-21c-.8 0-1.5.7-1.5 1.6 0 .9.7 1.6 1.5 1.6h21.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6zm-8 8.5h-13c-.8 0-1.5.7-1.5 1.6 0 .9.7 1.6 1.5 1.6h13.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6z"/></svg>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 portfolio sas">
                    <div class="portfolio_header">
                        <span class="portfolio_heading">MySAS</span>
                        <h3 class="portfolio_subheading">Cloud based enterprise resource planing system built on custom requirements for the brand SAS Sport</h3>
                    </div>
                    <div class="portfolio_media"></div>
                    <div class="portfolio_links">
                        <a class="portfolio_link" href=<?= $ini_array['path']."portfolio/sas"?>>
                            <span>Read story</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-288 379 35 35"><path d="M-260 386.5h-21c-.8 0-1.5.7-1.5 1.6s.7 1.6 1.5 1.6h21.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6zm0 8.4h-21c-.8 0-1.5.7-1.5 1.6 0 .9.7 1.6 1.5 1.6h21.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6zm-8 8.5h-13c-.8 0-1.5.7-1.5 1.6 0 .9.7 1.6 1.5 1.6h13.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6z"/></svg>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 portfolio tokocrypto">
                    <div class="portfolio_header">
                        <span class="portfolio_heading">TokoCrypto</span>
                        <h3 class="portfolio_subheading">Exchange platform for digital assets providing the benefits that blockchain technology can offer to the public</h3>
                    </div>
                    <div class="portfolio_media"></div>
                    <div class="portfolio_links">
                        <a class="portfolio_link" href=<?= $ini_array['path']."portfolio/tokocrypto"?>>
                            <span>Read story</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-288 379 35 35"><path d="M-260 386.5h-21c-.8 0-1.5.7-1.5 1.6s.7 1.6 1.5 1.6h21.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6zm0 8.4h-21c-.8 0-1.5.7-1.5 1.6 0 .9.7 1.6 1.5 1.6h21.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6zm-8 8.5h-13c-.8 0-1.5.7-1.5 1.6 0 .9.7 1.6 1.5 1.6h13.1c.8 0 1.5-.7 1.5-1.6s-.8-1.6-1.6-1.6z"/></svg>
                        </a>
                    </div>
                </div>
				<div class="row">
					<h2 class="sub_page_header text-center title">Web Design and <span>Development</span></h2>
					<div class="portfolio_section" data-aos="fade-up">
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Shangri La Hotel</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/shangriLaHotelColombo.jpg"?>>
								</div>
								<div class="back">
									<h3>Mass Recruitment Platform</h3>
									<p>This platform was developed to fullfill the mass paperless recruitment process requested by Shangri-La Hotel, Colombo.</p>
									<!--<a href="http://www.shangri-la.com/colombo/shangrila/" target="_blank" class="btn small btn-white-outline">View Project</a>-->
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Elephant Stables Hotels and Resorts</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/elephantStablesHotelsAndResorts.jpg"?>>
								</div>
								<div class="back">
									<h3>Hotels and Resorts Websites</h3>
									<p>The Sri Lankan chain of hotels worked with Redot to build their customized website. This website allows you to book any hotel coming under the Elephant Stables.</p>
									<a href="https://elephantstables.com/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Pooches Aromas</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/poochesAromas.jpg"?>>
								</div>
								<div class="back">
									<h3>E-commerce Platformt</h3>
									<p>Pooches Aromas is an e-commerce platform designed and developed for a pet-cosmetics brand in USA.</p>
									<a href="https://poochesaromas.com/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
					</div>
					<div class="portfolio_section" data-aos="fade-up">
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Ceylon My Dream</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/ceylonmyDream.jpg"?>>
								</div>
								<div class="back">
									<h3>Tourism Website</h3>
									<p>Srilankan tourism website focusing to capture the inbound tour market.</p>
									<a href="https://ceylonmydream.com/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Cepatswipe</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/cepatswipe.jpg"?>>
								</div>
								<div class="back">
									<h3>Mobile Application Website</h3>
									<p>Cepatswipe mobile application designed as a marketing platform enabled with the lock screen of the phone.</p>
									<a href="https://www.cepatswipe.com/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Mercantile Investment and Finance PLC</span>
							</div> 
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/mercantileInvestmentandFinancePlc.jpg"?>>
								</div>
								<div class="back">
									<h3>Support and Maintenance Services</h3>
									<p>Redot provides the support and maintenance services for Mercantile Investments official website.</p>
									<a href="http://mi.com.lk/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div> 
							</div>
						</div>
					</div>
					<div class="portfolio_section" data-aos="fade-up">
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Central Park Puppies</span>
							</div> 
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/centralParkPuppies.jpg"?>>
								</div>
								<div class="back">
									<h3>Pet Store Website</h3>
									<p>The USA based website allows its visitors to select and purchase puppies as their wish. The customers will be updated with the newly arrived puppies and their information frequently.</p>
									<a href="https://centralparkpuppies.com/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div> 
							</div>
						</div>
						<!--<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Thamaravila Resort</span>
							</div> 
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/thamaravilaResort.jpg"?>>
								</div>
								<div class="back">
									<h3>Thamaravila Resort Website</h3>
									<p>One of the most renowned resorts in Wilpattu, which is a sanctuary with major tourist attraction in Sri Lanka. We combined with them to create their online platform according to their requirements.</p>
									<a href="https://thamaravila.com/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div> 
							</div>
						</div>-->
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Amuseum Special Events</span>
							</div> 
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/amuseumse.jpg"?>>
								</div>
								<div class="back">
									<h3>Amuseum Special Events Website</h3>
									<p>Amuseum is among the leading nightlife entertainment organizations in Sri Lanka. Amuseum SE website created by Redot is focussed on their special events.</p>
									<a href="https://amuseumse.com/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div> 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<h2 class="sub_page_header text-center title">Web Application <span>Development</span></h2>
					<div class="portfolio_section" data-aos="fade-up">
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Venuerific</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/venuerific.jpg"?>>
								</div>
								<div class="back">
									<h3>Event Venue Booking</h3>
									<p>One of the Singapore's largest booking platform specifically targeting the event venues with advanced customizations.</p>
									<a href="https://www.venuerific.com" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Simply Rate</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/simplyRate.jpg"?>>
								</div>
								<div class="back">
									<h3>Business Review platform</h3>
									<p>An advanced review platform designed for businesses in global market which will be launched soon in USA.</p>
									<!-- <a href="#" class="btn small btn-white-outline">View Project</a> -->
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Petpos</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/petpos.jpg"?>>
								</div>
								<div class="back">
									<h3>Pet Store Managament Application</h3>
									<p>A cloud based ERP application which designed specifically to manage pet shops and their sales.</p>
									<!-- <a href="#" class="btn small btn-white-outline">View Project</a> -->
								</div>
							</div>
						</div>
					</div>
					<div class="portfolio_section" data-aos="fade-up">
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Recover Pet</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/recoverPet.jpg"?>>
								</div>
								<div class="back">
									<h3>Recover Lost Pet Application</h3>
									<p>The system offers a location- based service in recovering the lost dogs. It is based in US.</p>
									<!--<a href="#" target="_blank" class="btn small btn-white-outline">View Project</a>-->
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<h2 class="sub_page_header text-center title">SEO and <span>SMM</span></h2>
					<div class="portfolio_section" data-aos="fade-up">
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">BMW</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/bmw.jpg"?>>
								</div>
								<div class="back">
									<h3>SEO and SMM BMW Social Media</h3>
									<a href="https://www.facebook.com/BMWSriLanka/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">Porsche</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/porsche.jpg"?>>
								</div>
								<div class="back">
									<h3>SEO and SMM Porsche Social Media</h3>
									<a href="https://www.facebook.com/porschesrilanka/" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
						<div class="card-wrapper col-lg-4 col-md-6">
							<div class="portfolio_header">
								<span class="portfolio_heading">MINI Cooper</span>
							</div>
							<div class="card">
								<div class="front"> 
									<img src=<?= $ini_array['path']."assets/images/mini.jpg"?>>
								</div>
								<div class="back">
									<h3>SEO and SMM MINI Cooper Social Media</h3>
									<a href="https://www.facebook.com/MINISriLanka" target="_blank" class="btn small btn-white-outline">View Project</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4"></div>
				</div>
            </div>
        </div>
        <div class="footer-cta">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h2 class="title">Ready to get started?</h2>    
                        </div>
                        <div class="col-md-8">
                            <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                            <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('../_partials/footer.php'); ?>  