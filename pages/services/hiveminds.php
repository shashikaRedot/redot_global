<?php
$pageTitle = 'Digital Strategy | Services | Redot Software Solutions';
$pageMetaDescription = 'Right Digital Strategy will boost your profits irrespective of what business you are doing. So, our digital strategists will be at your service always in creating the ideal customized strategy for your business.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot digital strategy">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Redot provides you a wide range of end to end services fulfilling all your online requirements</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Services</li>
                        <li class="active">HiveMinds</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <h1 class="sub_page_header text-center title text-large">HiveMinds <span>Service</span></h1>
                <p class="text-center">Redot is one of the best companies located in Sri Lanka which provides total digital solutions at your convenience. We support your company to establish a successful online landmark with our comprehensive digital strategy consultancy.</p>
                <div class="services-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/rightGuyForTheRightJob.jpg"?> alt="redot right guy for the right job">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Right Guy for the Right Job</h2>
                                <p>Though our staff is multitalented, we will always recommend you a connoisseur to meet your requirements depending on what you seek.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/dedicatedInhouseTeams.jpg"?> alt="redot dadicated inhouse teams">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Flexibility</h2>
                                <p>We have 20+ employees who are willing to work at your shifts making it not burdensome nor complicated for you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/idealForAnyCompany.jpg"?> alt="redot ideal for any company">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Ideal for any company</h2>
                                <p>Irrespective of your company size our dedicated In-house scrum teams will help you achieve what you wish for. Start-ups highlighted, you don’t have to anxious anymore.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/worthForYourMoney.jpg"?> alt="worth for your investment">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Worth for your Money</h2>
                                <p>Our Scrum teams have always exceeded the expectations of our clients. It will be smooth partnership between us giving you the max worth for your investment.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  