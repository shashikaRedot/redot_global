<?php
$pageTitle = 'System Integration | Services | Redot Software Solutions';
$pageMetaDescription = 'We will make sure your smart decision to integrate your software gets implemented. Be it ERP, CRM, Social media or Accounting software Integration, we do them all. You just demonstrate your requirements and our team will fulfill them for you.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot system integration">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Redot provides you a wide range of end to end services fulfilling all your online requirements</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Services</li>
                        <li class="active">System Integration</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <h1 class="sub_page_header text-center title text-large">System<span> Integration</span></h1>
                <p class="text-center">Integrating all your business functions to a main web solution will definitely make your day to day activities smoother. We, Redot will help you creating your business systems and integrate them with your web solution.</p>
                <div class="services-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/customSystemIntegration.jpg"?> alt="Redot will help you to integrate your multiple software solutions">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Custom System Integration</h2>
                                <p>Undoubtedly, you must be working with specific software solutions in your day to day business activities such as Xero, Myob, eWay, Salesforce and social media platforms. Wouldn’t it be faster and smarter if you could integrate all together? </p>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/hybridApps.jpg"?> alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Hybrid Apps</h2>
                                <p>Hybrid Apps are built to be compatible with most of the mobile operating systems. If you are looking for a cost friendly solution, our team can provide you a better hybrid solution.</p>
                            </div>
                        </div>
                    </div>-->
                    <!--<div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/paymentGatewayIntegration.jpg"?> alt="redot payment gateway integration">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Payment Gateway Integration</h2>
                                <p>We can help you create a digital payment system linked with your bank which will help you to generate more business via your digital solution.</p>
                            </div>
                        </div>
                    </div>-->
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/accountingSoftwareIntegration.jpg"?> alt="Now you can integrate your accounting system through Redot">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Accounting Software Integration</h2>
                                <p>Redot team gives you a range of solutions to integrate your accounting system with your website so that you don’t have to enter everything manually.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/customerRelationshipManagementIntegration.jpg"?> alt="We enable the CRM integration through your website">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>CRM Integration</h2>
                                <p>Would you like being capable of connecting directly and smoothly with your customer relationship management system through your web site?</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/socialMediaIntegration.jpg"?> alt="Redot facilitates your communication with the customers through social media integration">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Social Media Integration</h2>
                                <p>One of the trendiest ways to reach and create an interaction with your customers is to reach them through social media in an interactive way. Team Redot has its own specialists of creating social media integrations to your current web site. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  