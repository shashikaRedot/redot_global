<?php
$pageTitle = 'Digital Marketing | Services | Redot Software Solutions';
$pageMetaDescription = 'We are implementing globally tested and successful strategies to overcome your marketing concerns. Let us handle your Social Media marketing, PPC services, SEO strategies and copywriting services as we are experts here.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot digital marketing">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Redot provides you a wide range of end to end services fulfilling all your online requirements</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Services</li>
                        <li class="active">Digital Marketing</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <h1 class="sub_page_header text-center title text-large">Digital<span> Marketing</span></h1>
                <p class="text-center">Voice your brand to the world. From the first step of having a website to the next step of attracting customers around the brand. 360 degree digital marketing solutions.</p>
                <div class="services-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src="../assets/images/socialMediaMarketing.jpg" alt="Redot will provide your social media marketing solutions">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Social Media Marketing</h2>
                                <p>Are you ready to take up the challenge of going beyond your traditional market and creating a brand community? Let Social Media do the magic. We provide you extensive social media solutions for your business including campaign ideation, content creation and real-time monitoring on all the available social media platforms.</p>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters">
                            <div class="service-block-media">
                                <img src="../assets/images/hybridApps.jpg" data-aos="fade" alt="redot hybrid apps">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Hybrid Apps</h2>
                                <p>Hybrid Apps are built to be compatible with most of the mobile operating systems. If you are looking for a cost friendly solution, our team can provide you a better hybrid solution.</p>
                            </div>
                        </div>
                    </div>-->
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src="../assets/images/conversionRateIdealization.jpg" alt="Redot will take steps to create traffic towards your website">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Conversion rate idealization</h2>
                                <p>Having a website is primary, creating traffic towards the site and givinga user-friendly service while converting prospects to customers is the next challenge. That’s what our team would do.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src="../assets/images/ppcServices.jpg" alt="Start your PPC advertising campaign now with Redot">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>PPC Services</h2>
                                <p>One of the most cost effective ways of advertising on web is PPC Advertising. Our digital team can provide well targeted PPC Advertising campaign solutions from the ideation to the deployment.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src="../assets/images/seoService.jpg" alt="Redot’s SEO strategies will give your content a good exposure">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>SEO Service</h2>
                                <p>Marketing your site is as much as important as marketing your brand. Our team can take up the challenge of marketing your site by deploying a customized SEO strategy.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src="../assets/images/copywritingServices.jpg" alt="Creative and SEO friendly content by Redot Copywriters">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Copywriting Services</h2>
                                <p>What’s worse than bad is bad content. Having trouble creating content? Let our team worry about that. We create well focused, SEO friendly content to energize the conversion rate.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  