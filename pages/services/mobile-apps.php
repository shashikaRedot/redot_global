<?php
$pageTitle = 'Mobile Apps | Services | Redot Software Solutions';
$pageMetaDescription = 'We are capable of creating both native windows, android or ios mobile apps and cost effective hybrid apps as well for your business. According to your choice, our app specialists will be at your service.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot mobile apps">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Redot provides you a wide range of end to end services fulfilling all your online requirements</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Services</li>
                        <li class="active">Mobile Apps</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <h1 class="sub_page_header text-center title text-large">Mobile<span> Apps</span></h1>
                <p class="text-center">When everything is getting into Apps around you, isn’t it necessary for you to make your business available on App? Making your business available as an App will definitely give your customers a unique brand experience while helping you to be ahead of the competition.</p>
                <div class="services-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/systemIntegration.jpg"?> alt="Redot developers are specialists in creating native windows, android and ios apps">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Native Windows, Android & iOS Apps</h2>
                                <p>App created for a specific operating system is called a native app. These are known to have a better user interface and are easy to discover in stores. Team Redot is blessed with Mobile App specialists.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/hybridApps.jpg"?> alt="Hybrid apps developed by Redot will come in handy if you want cost friendly solutions">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Hybrid Apps</h2>
                                <p>Hybrid Apps are built to be compatible with most of the mobile operating systems. If you are looking for a cost friendly solution, our team can provide you a better hybrid solution.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/mobileBrandVisibility.jpg"?> alt="We provide attractive interfaces energizing the visibility of your brand">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Mobile Brand Visibility</h2>
                                <p>Apps can be identified as one of the best visibility energizers for your brand. It creates a long lasting impact on the brand once it’s installed. We will provide you the most attractive interface to captivate your customers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/betterPurchaseExperie.jpg"?> alt="Your customers will have the best purchase experience when working with apps built by Redot">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Better Purchase Experience</h2>
                                <p>More than a web page, a mobile app can influence more purchases. All the customers are keen to feel special while they are making their purchase. Well, Redot will create the best purchase experience for your customers.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  