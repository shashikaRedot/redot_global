<?php
$pageTitle = 'Web Design and Development | Services | Redot Software Solutions';
$pageMetaDescription = 'All web services using modern technologies at an affordable rate. Redot provide you custom web solutions focusing on mobile first UI/UX design helping you to achieve your business goals.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot web design and development">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Redot provides you a wide range of end to end services fulfilling all your online requirements</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Services</li>
                        <li class="active">Web Design and Development</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <h1 class="sub_page_header text-center title text-large">Web <span>Design and Development</span></h1>
                <p class="text-center">We have quality references in developing custom web solutions, our clients entrust in achieving their long and short business goals.</p>
                <div class="services-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/mobileFirstUi.jpg"?> alt="Redot are focusing on Mobile First UI/UX design in Web design and Development">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Mobile First UI /UX Design</h2>
                                <p>Today everyone is handy with a mobile device in their hand. When we are crafting our web solution interface always the priority is given to the convenience of the user/ experiencers. Keeping that on top, latest technologies are used to customize your business requirements.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/customOnlineApplications.jpg"?> alt="This is your website, so we give you the privilege to customize your website in its development and design">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Custom Online Applications</h2>
                                <p>Most of the companies are using Online Applications tied up to theirPromotional and other strategies. Our set of skilled developers will completely take your requirements into consideration and will create a unique solution surpassing your expectations.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/hackerProofCoding.jpg"?> alt="Redot provide you the much needed security solutions to your website through hacker-proof coding and other techniques">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Hacker-Proof Coding</h2>
                                <p>Security of your information is our main priority as everything else. Our team can facilitate you with the most sophisticated security solutions available in the world just for the protection of your information.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/highlyScalableDatabaseImplementation.jpg"?> alt="Scalable database will prove to be beneficial when you are expanding your business">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Highly Scalable Database Implementation</h2>
                                <p>Scalable Database is a concept where you will be facilitated with the ability to adopt growth of data and users without a problem. In the modern application sphere this is one of the must-have abilities in your application. Let our team worry about creating one for you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/searchEngineFriendly.jpg"?> alt="Search Engine Optimization (SEO) will make your web applications more discoverable to the public">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Search Engine Friendly</h2>
                                <p>Did you know that it’s important for the search engines to get to know aboutyour web applications better? This will improve the customer awareness aboutyour brand. Our SEO specialists will make sure that your web application willbe much friendlier with all the search engines.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  