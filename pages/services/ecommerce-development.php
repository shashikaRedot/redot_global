<?php
$pageTitle = 'ECommerce Development | Services | Redot Software Solutions';
$pageMetaDescription = 'Adopt the applications of Ecommerce and get in the same track as your strong market competitors. Create your Ecommerce platform now with Redot and make your business an E business';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot ecommerce development">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Redot provides you a wide range of end to end services fulfilling all your online requirements</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Services</li>
                        <li class="active">Ecommerce Development</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <h1 class="sub_page_header text-center title text-large">Ecommerce <span>Development</span></h1>
                <p class="text-center">We all left the traditional market place a decade ago to a more convenient digital market space. Being a modern competitive business it is necessary to have an eCommerce platform.</p>
                <div class="services-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/interactiveUiUxDesign.jpg"?> alt="Redot allows your customers to have the best UI/UX interface experience">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Interactive UI/UX Design</h2>
                                <p>Going beyond just proving information to the customers it is always important to create a real-time conversation which will impact on a fruitful conversion. We will make sure your customers will experience the best interactive UI/UX designs.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/userFriendlyBackend.jpg"?> alt="We create a user friendly backend making it easy for you to modify the content">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>User Friendly Backend</h2>
                                <p>We will ensure that even you will be privileged and capable to update content whenever necessary with our super simple user backend.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/paymentGatewayIntegration.jpg"?> alt="We integrate your Ecommerce website with secured payment gateways">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Payment Gateway Integration</h2>
                                <p>What matters the most for a quality eCommerce solution is a secured payment gateway. Our team will guarantee that you will experience the smoothest payment gateway integration.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/3rdPartySystemIntegration.jpg"?> alt="As per your requirements Redot will provide you 3rd party system integrations as well">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>3<sup>rd</sup> Party System Integration</h2>
                                <p>Not only the payment gateways, but also other business system integrations including store management systems, customer relationship management system and accounting systems will be provided by our Redot team.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/realTimeNotificationUpdates.jpg"?> alt="Real-time notifications will keep you updated about your website">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Real-time Notification Updates</h2>
                                <p>Worried about missing opportunities due to late notifications? We will make sure that you will be synced with the system and receive real time notifications ensuing you won’t miss out on anything.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  