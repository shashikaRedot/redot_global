<?php
$pageTitle = 'Managed Cloud Services | Services | Redot Software Solutions';
$pageMetaDescription = 'As a progressing business you will have to select the cloud solutions wisely. Forget your tension about the best platforms and leave it on us. We are masters in providing dedicated servers, VPS hosting and AWS/ Microsoft AZURE/ Heroku Consultancy.';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot managed cloud services">
            <div class="container">
                <h2 class="title">Services</h2>
                <hr>
                <p>Redot provides you a wide range of end to end services fulfilling all your online requirements</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Services</li>
                        <li class="active">Managed Cloud Services</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="about-us page">
            <div class="container">
                <h1 class="sub_page_header text-center title text-large">Managed<span> Cloud Services</span></h1>
                <p class="text-center">Why store everything on your hard drive time to time and take the risk of losing data? Team Redot will provide you a unique and a more secure cloud solution for your business.</p>
                <div class="services-block">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/dedicatedServers.jpg"?> alt="Dedicated Servers with effective response times">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>Dedicated Servers</h2>
                                <p>Having trouble due to system delays caused by the web servers? We, Redot provide you 100% dedicated servers just for you with the full capacity. Improve your system response time with dedicated cloud services.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/vpsHosting.jpg"?> alt="Redot will assist you in selecting the right VPS for hosting">
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>VPS Hosting</h2>
                                <p>Virtual Private Server has its own operating system which facilitate you to install custom software as per your need. Redot team will make sure that you select the right hosting service and will manage from the beginning to the end of the process.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-media">
                                <img src=<?= $ini_array['path']."assets/images/awsMicrosoftAzureHerokuConsultancy.jpg"?> alt="Redot will consult and guide you in selecting the best cloud platform solutions">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 no-gutters" data-aos="fade">
                            <div class="service-block-text">
                                <h2>AWS/ Microsoft AZURE/ Heroku Consultancy</h2>
                                <p>To cater your most reliable and most secure cloud platform solutions handled by the top global providers, team Redot will be at your disposal to consult you and guide you throughout the process.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="title">Ready to get started?</h2>    
                    </div>
                    <div class="col-md-8">
                        <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                        <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('../../_partials/footer.php'); ?>  