<?php include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src="../assets/images/pages/about_us.jpg">
            <div class="container">
                <h2 class="title">Pricing</h2>
                <hr>
                <p>We've been identifying opportunities and delivering solutions that people love to use for more than 15 years.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Pricing</li>
                        <li class="active">SSM</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="packages page">
            <div class="container">
                <div class="web-package">
                    <h2 class="sub_page_header text-center title">Social Media Marketing <span>Packages</span></h2>
                    <table class="smm-pricing-table desktop">
                        <tbody>
                            <tr class="header">
                                <td></td>
                                <td><h3>Local</h3></td>
                                <td><h3>Bronze</h3></td>
                                <td><h3>Silver</h3></td>
                                <td><h3>Gold</h3></td>
                                <td><h3>Platinum</h3></td>
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Campaign Setup</td>
                            </tr>
                            <tr>
                                <td>In-depth Site Analysis</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Content Duplicacy Check</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Initial Backlinks analysis</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                  
                            </tr>
                            <tr>
                                <td>Google Penalty Check</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Mobile Usability Check</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Competition Analysis</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">On-Page Optimization</td>
                            </tr>
                            <tr>
                                <td>Title & Meta Tags Optimization</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Content Optimization</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Mobile site optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                  
                            </tr>
                            <tr>
                                <td>Page Speed Analysis & Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>HTML Code Cleanup & Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Internal Link Structuring & Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                        </tbody>
                    </table>
                    <div class="smm-pricing-div row">
                        <div class="col-sm-12">
                            <table class="smm-pricing-table mobile">
                                    <tbody>
                                        <tr class="header">
                                            <td></td>
                                            <td><h3>Local</h3></td>
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Campaign Setup</td>
                                        </tr>
                                        <tr>
                                            <td>In-depth Site Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Content Duplicacy Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Initial Backlinks analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Google Penalty Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Mobile Usability Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Competition Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">On-Page Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>Title & Meta Tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Content Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Mobile site optimization</td>
                                            <td>&nbsp;</td>                                 
                                        </tr>
                                        <tr>
                                            <td>Page Speed Analysis & Optimization</td>
                                            <td>&nbsp;</td>                               
                                        </tr>
                                        <tr>
                                            <td>HTML Code Cleanup & Optimization</td>
                                            <td>&nbsp;</td>                              
                                        </tr>
                                        <tr>
                                            <td>Internal Link Structuring & Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-12"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-cta">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h2 class="title">Ready to get started?</h2>    
                        </div>
                        <div class="col-md-8">
                            <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                            <a href="#" class="btn btn-white-outline">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('../../_partials/footer.php'); ?>     