<?php
$pageTitle = 'Website | Pricing | Redot Software Solutions';
$pageMetaDescription = 'Build your personal or business website with Redot according to your requirements. We design interactive and responsive websites at affordable price ranges';
include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src=<?= $ini_array['path']."assets/images/pages/about_us.jpg"?> alt="redot website development">
            <div class="container">
                <h2 class="title">Pricing</h2>
                <hr>
                <p>We've been identifying opportunities and delivering solutions that people love to use for more than 15 years.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Pricing</li>
                        <li class="active">Website</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="packages page">
            <div class="container">
                <div class="web-package" data-aos="fade-up">
                    <h2 class="sub_page_header text-center title">Website Development <span>Packages</span></h2>
                    <table class="web-pricing-table desktop">
                        <tbody>
                        <tr class="header">
                            <td>
                                <h3>Personal</h3>
                                <div class="table-mini-sep"></div>
                                <h3>From <span class="brand-color">USD 750</span> Per Website</h3>
                            </td>
                            <td data-banner="Recommended" class="best">
                                <h3>Business</h3>
                                <div class="table-mini-sep"></div>
                                <h3>From <span class="brand-color">USD 1499</span> Per Website</h3>
                            </td>
                            <td>
                                <h3>Ultimate</h3>
                                <div class="table-mini-sep"></div>
                                <h3>From <span class="brand-color">USD 2799</span> Per Website</h3>
                            </td>                            
                        </tr>
                        <tr class="p-bottom">
                            <td>
                                <h4>3 Custom Designed Pages</h4>
                                <p>3 custom design web pages</p>
                                <p>+ Additional 5 pages</p>
                            </td>
                            <td>
                                <h4>Everything from Personal Package</h4>
                                <p>5 custom design web pages</p>
                                <p>+ Additional 15 pages</p>
                            </td>
                            <td>
                                <h4>Everything from Business Package</h4>
                                <p>15 custom design web pages</p>
                                <p>+ Any number of additional pages</p>
                            </td>
                        </tr>
                        <tr class="p-bottom">
                            <td>
                                <h4>Mobile Friendly</h4>
                                <p>Mobile first design approach</p>
                                <p>Fully responsive web pages</p>
                            </td>
                            <td>
                                <h4>CMS Built in</h4>
                                <p>Manage your content from admin panel</p>
                                <p>Make customizations to your website</p>
                            </td>
                            <td>
                                <h4>Premium Hosting</h4>
                                <p>Premium web shoting included</p>
                                <p>Top data centers to support high traffic websites</p>
                            </td>
                        </tr>
                        <tr class="p-bottom">
                            <td>
                                <h4>Search Engine Optimization</h4>
                                <p>Basic search engine optimization in-built</p>
                                <p>Google analytics installed</p>
                            </td>
                            <td>
                                <h4>Advanced SEO</h4>
                                <p>Optimized source code</p>
                                <p>Our premium seo plugin built in</p>
                            </td>
                            <td>
                                <h4>Social Media Integration</h4>
                                <p>Connect your social media accounts to the website</p>
                                <p>Post to social media platforms from your website</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="web-pricing-div row">
                        <div col-sm-4>
                            <table class="web-pricing-table mobile">
                                <tbody>
                                    <tr class="header">
                                        <td>
                                            <h3>Personal</h3>
                                            <div class="table-mini-sep"></div>
                                            <h3>From <span class="brand-color">USD 750</span> Per Website</h3>
                                        </td>                     
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4><i class="fas fa-check-square-o" aria-hidden="true"></i> 3 Custom Designed Pages</h4>
                                            <p>3 custome design web pages</p>
                                            <p>+ Additional 5 pages</p>
                                        </td>
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>Mobile Friendly</h4>
                                            <p>Mobile first design approach</p>
                                            <p>Fully responsive web pages</p>
                                        </td>
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>Search Engine Optimization</h4>
                                            <p>Basic search engine optimization in-built</p>
                                            <p>Google analytics installed</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div col-sm-4>
                            <table class="web-pricing-table mobile">
                                <tbody>
                                    <tr class="header">
                                        <td data-banner="Recommended" class="best">
                                            <h3>Business</h3>
                                            <div class="table-mini-sep"></div>
                                            <h3>From <span class="brand-color">USD 1499</span> Per Website</h3>
                                        </td>                         
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>Everything from Personal Package</h4>
                                            <p>5 custome design web pages</p>
                                            <p>+ Additional 15 pages</p>
                                        </td>
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>CMS Built in</h4>
                                            <p>Manage your content from admin panel</p>
                                            <p>Make customizations to your website</p>
                                        </td>
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>Advanced SEO</h4>
                                            <p>Optimized source code</p>
                                            <p>Our premium seo plugin built in</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div col-sm-12>
                            <table class="web-pricing-table mobile">
                                <tbody>
                                    <tr class="header">
                                        <td>
                                            <h3>Ultimate</h3>
                                            <div class="table-mini-sep"></div>
                                            <h3>From <span class="brand-color">USD 2799</span> Per Website</h3>
                                        </td>                            
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>Everything from Business Package</h4>
                                            <p>15 custome design web pages</p>
                                            <p>+ Any number of additional pages</p>
                                        </td>
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>Premium Hosting</h4>
                                            <p>Premium web shoting included</p>
                                            <p>Top data centers to support high traffic websites</p>
                                        </td>
                                    </tr>
                                    <tr class="p-bottom">
                                        <td>
                                            <h4>Social Media Integration</h4>
                                            <p>Connect your social media accounts to the website</p>
                                            <p>Post to social media platforms from your website</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-cta">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h2 class="title">Ready to get started?</h2>    
                        </div>
                        <div class="col-md-8">
                            <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                            <a href="/contact" class="btn btn-white-outline">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('../../_partials/footer.php'); ?>     