<?php include('../../_partials/header.php'); ?>
<?php include('../../_partials/menu.php'); ?>
    <div class="wrapper home">
        <div class="page_header about_us tint-dark" data-parallax="scroll" data-image-src="../assets/images/pages/about_us.jpg">
            <div class="container">
                <h2 class="title">Pricing</h2>
                <hr>
                <p>We've been identifying opportunities and delivering solutions that people love to use for more than 15 years.</p>
            </div>
        </div>
        <div class="sub_nav">
            <div class="col-md-12 utility_bar">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Welcome to Redot</li>
                        <li>Pricing</li>
                        <li class="active">SSM</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="packages page">
            <div class="container">
                <div class="web-package">
                    <h2 class="sub_page_header text-center title">Social Media Marketing <span>Packages</span></h2>
					<div class="tableFixHead">
                    <table class="smm-pricing-table desktop">
                        <tbody>
							<tr class="header">
                                <td></td>
                                <td><h3>Local</h3></td>
                                <td><h3>Bronze</h3></td>
                                <td><h3>Silver</h3></td>
                                <td><h3>Gold</h3></td>
                                <td><h3>Platinum</h3></td>
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Campaign Setup</td>
                            </tr>
                            <tr>
                                <td>In-depth Site Analysis</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Content Duplicacy Check</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Initial Backlinks analysis</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                  
                            </tr>
                            <tr>
                                <td>Google Penalty Check</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Mobile Usability Check</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Competition Analysis</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Keyword Research</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Targeted Keywords Count</td>
                                <td>5</td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td>50</td>                                 
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">On-Page Optimization</td>
                            </tr>
                            <tr>
                                <td>Title & Meta Tags Optimization</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Content Optimization</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Mobile site optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                  
                            </tr>
                            <tr>
                                <td>Page Speed Analysis & Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>HTML Code Cleanup & Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Internal Link Structuring & Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Pages H tags Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Canonicalization/301 Redirect</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Schema Markup Implementation</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Image & Hyperlink Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Image & Hyperlink Optimization</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Robots.txt Creation/Analysis</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Sitemap Creation</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Google & Bing Webmaster Tools Setup</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr>
                                <td>Google Analytics Setup & Integration</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                 
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Content Marketing and Link Acquisition (Per Month)</td>
                            </tr>
                            <tr>
                                <td>Blog Articles</td>
                                <td>1</td>
                                <td>1</td>
                                <td>2</td>
                                <td>4</td>
                                <td>8</td>                                
                            </tr>
                            <tr>
                                <td>Informational Content Writing & Sharing</td>
                                <td>1</td>
                                <td>2</td>
                                <td>2</td>
                                <td>4</td>
                                <td>6</td>                                
                            </tr>
                            <tr>
                                <td>Guest Blog Outreach</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Magazine / News Placement</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Q&A Posting</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Premium PR Distribution</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Infographic Creation & Distribution</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Social Media Marketing (Per Month)</td>
                            </tr>
                            <tr>
                                <td>Google Plus Page Setup</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Facebook Page Setup</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Twitter Account Setup</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Instagram Account Setup</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Youtube Account Setup</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Monitoring and responses to social activity</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Unique Social Media Posting</td>
                                <td>2</td>
                                <td>5</td>
                                <td>8</td>
                                <td>12</td>
                                <td>15</td>                                
                            </tr>
                            <tr>
                                <td>Facebook Ads Management</td>
                                <td>40USD</td>
                                <td>80USD</td>
                                <td>120USD</td>
                                <td>200USD</td>
                                <td>450USD</td>                                
                            </tr>
                            <tr>
                                <td>Google AdWords Management</td>
                                <td>50USD</td>
                                <td>100USD</td>
                                <td>150USD</td>
                                <td>250USD</td>
                                <td>500USD</td>                                
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Video Marketing (Per Month)</td>
                            </tr>
                            <tr>
                                <td>Video / PPT Creation + Distribution</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>On Request</td>
                                <td>1</td>
                                <td>3</td>                                
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Local Search Optimization</td>
                            </tr>
                            <tr>
                                <td>NAP Syndication</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Google My Business / Bing Local Listing</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Updating Pages & Schema Integration</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Classified Submissions</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Coupon Distribution</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Monthly Reporting</td>
                            </tr>
                            <tr>
                                <td>Search Engine Rank Report</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>SEO Reports</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Google Analytics Analysis Report</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Activity Report</td>
                                <td>&nbsp;</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>Monthly Action Plan</td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Customer Support</td>
                            </tr>
                            <tr>
                                <td>Email/ Chat/ Telephone</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>On Request</td>
                                <td>On Request</td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr>
                                <td>PT Client Center (24/7 Live Project Tracking)</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>On Request</td>
                                <td>On Request</td>
                                <td><i class="fas fa-check"></i></td>                                
                            </tr>
                            <tr class="sub-head">
                                <td colspan="7">Price</td>
                            </tr>
                            <tr>
                                <td>Monthly Cost (LKR)</td>
                                <td>35000</td>
                                <td>55000</td>
                                <td>80000</td>
                                <td>135000</td>
                                <td>350000</td>                                
                            </tr>
                        </tbody>
                    </table>
					</div>
                    <div class="smm-pricing-div row">
                        <div class="col-sm-12">
                            <table class="smm-pricing-table mobile">
                                    <tbody>
                                        <tr class="header">
                                            <td></td>
                                            <td><h3>Local</h3></td>
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Campaign Setup</td>
                                        </tr>
                                        <tr>
                                            <td>In-depth Site Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Content Duplicacy Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Initial Backlinks analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Google Penalty Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Mobile Usability Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Competition Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Keyword Research</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Targeted Keywords Count</td>
                                            <td>5</td>                               
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">On-Page Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>Title & Meta Tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Content Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Mobile site optimization</td>
                                            <td>&nbsp;</td>                                 
                                        </tr>
                                        <tr>
                                            <td>Page Speed Analysis & Optimization</td>
                                            <td>&nbsp;</td>                               
                                        </tr>
                                        <tr>
                                            <td>HTML Code Cleanup & Optimization</td>
                                            <td>&nbsp;</td>                              
                                        </tr>
                                        <tr>
                                            <td>Internal Link Structuring & Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Pages H tags Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Canonicalization/301 Redirect</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Schema Markup Implementation</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Image & Hyperlink Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Robots.txt Creation/Analysis</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Sitemap Creation</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Google & Bing Webmaster Tools Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Setup & Integration</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Content Marketing and Link Acquisition (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Blog Articles</td>
                                            <td>1</td>                                
                                        </tr>
                                        <tr>
                                            <td>Informational Content Writing & Sharing</td>
                                            <td>1</td>                                
                                        </tr>
                                        <tr>
                                            <td>Guest Blog Outreach</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Magazine / News Placement</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Q&A Posting</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Premium PR Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Infographic Creation & Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Social Media Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Google Plus Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Twitter Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Instagram Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Youtube Account Setup</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Monitoring and responses to social activity</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Unique Social Media Posting</td>
                                            <td>2</td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Ads Management</td>
                                            <td>40USD</td>                                
                                        </tr>
                                        <tr>
                                            <td>Google AdWords Management</td>
                                            <td>50USD</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Video Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Video / PPT Creation + Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Local Search Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>NAP Syndication</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google My Business / Bing Local Listing</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Updating Pages & Schema Integration</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Classified Submissions</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Coupon Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Monthly Reporting</td>
                                        </tr>
                                        <tr>
                                            <td>Search Engine Rank Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>SEO Reports</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Analysis Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Activity Report</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Monthly Action Plan</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Customer Support</td>
                                        </tr>
                                        <tr>
                                            <td>Email/ Chat/ Telephone</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>PT Client Center (24/7 Live Project Tracking)</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Price</td>
                                        </tr>
                                        <tr>
                                            <td>Monthly Cost (LKR)</td>
                                            <td>35000</td>                                
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="smm-pricing-table mobile">
                                    <tbody>
                                        <tr class="header">
                                            <td></td>
                                            <td><h3>Bronze</h3></td>
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Campaign Setup</td>
                                        </tr>
                                        <tr>
                                            <td>In-depth Site Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Content Duplicacy Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Initial Backlinks analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Google Penalty Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Mobile Usability Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Competition Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Keyword Research</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Targeted Keywords Count</td>
                                            <td>10</td>                               
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">On-Page Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>Title & Meta Tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Content Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Mobile site optimization</td>
                                            <td>&nbsp;</td>                                 
                                        </tr>
                                        <tr>
                                            <td>Page Speed Analysis & Optimization</td>
                                            <td>&nbsp;</td>                               
                                        </tr>
                                        <tr>
                                            <td>HTML Code Cleanup & Optimization</td>
                                            <td>&nbsp;</td>                              
                                        </tr>
                                        <tr>
                                            <td>Internal Link Structuring & Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Pages H tags Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Canonicalization/301 Redirect</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Schema Markup Implementation</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Image & Hyperlink Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Robots.txt Creation/Analysis</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Sitemap Creation</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google & Bing Webmaster Tools Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Setup & Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Content Marketing and Link Acquisition (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Blog Articles</td>
                                            <td>1</td>                                
                                        </tr>
                                        <tr>
                                            <td>Informational Content Writing & Sharing</td>
                                            <td>2</td>                                
                                        </tr>
                                        <tr>
                                            <td>Guest Blog Outreach</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Magazine / News Placement</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Q&A Posting</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Premium PR Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Infographic Creation & Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Social Media Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Google Plus Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Twitter Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Instagram Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Youtube Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Monitoring and responses to social activity</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Unique Social Media Posting</td>
                                            <td>5</td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Ads Management</td>
                                            <td>80USD</td>                                
                                        </tr>
                                        <tr>
                                            <td>Google AdWords Management</td>
                                            <td>100USD</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Video Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Video / PPT Creation + Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Local Search Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>NAP Syndication</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google My Business / Bing Local Listing</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Updating Pages & Schema Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Classified Submissions</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Coupon Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Monthly Reporting</td>
                                        </tr>
                                        <tr>
                                            <td>Search Engine Rank Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>SEO Reports</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Analysis Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Activity Report</td>
                                            <td><i class="fas fa-check"></i</td>                                
                                        </tr>
                                        <tr>
                                            <td>Monthly Action Plan</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Customer Support</td>
                                        </tr>
                                        <tr>
                                            <td>Email/ Chat/ Telephone</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>PT Client Center (24/7 Live Project Tracking)</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Price</td>
                                        </tr>
                                        <tr>
                                            <td>Monthly Cost (LKR)</td>
                                            <td>55000</td>                                
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="smm-pricing-table mobile">
                                    <tbody>
                                        <tr class="header">
                                            <td></td>
                                            <td><h3>Silver</h3></td>
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Campaign Setup</td>
                                        </tr>
                                        <tr>
                                            <td>In-depth Site Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Content Duplicacy Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Initial Backlinks analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Google Penalty Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Mobile Usability Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Competition Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Keyword Research</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Targeted Keywords Count</td>
                                            <td>15</td>                               
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">On-Page Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>Title & Meta Tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Content Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Mobile site optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Page Speed Analysis & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>HTML Code Cleanup & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Internal Link Structuring & Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Pages H tags Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Canonicalization/301 Redirect</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Schema Markup Implementation</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Image & Hyperlink Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Robots.txt Creation/Analysis</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Sitemap Creation</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google & Bing Webmaster Tools Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Setup & Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Content Marketing and Link Acquisition (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Blog Articles</td>
                                            <td>2</td>                                
                                        </tr>
                                        <tr>
                                            <td>Informational Content Writing & Sharing</td>
                                            <td>2</td>                                
                                        </tr>
                                        <tr>
                                            <td>Guest Blog Outreach</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Magazine / News Placement</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Q&A Posting</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Premium PR Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Infographic Creation & Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Social Media Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Google Plus Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Twitter Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Instagram Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Youtube Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Monitoring and responses to social activity</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Unique Social Media Posting</td>
                                            <td>8</td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Ads Management</td>
                                            <td>120USD</td>                                
                                        </tr>
                                        <tr>
                                            <td>Google AdWords Management</td>
                                            <td>150USD</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Video Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Video / PPT Creation + Distribution</td>
                                            <td>On Request</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Local Search Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>NAP Syndication</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google My Business / Bing Local Listing</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Updating Pages & Schema Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Classified Submissions</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Coupon Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Monthly Reporting</td>
                                        </tr>
                                        <tr>
                                            <td>Search Engine Rank Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>SEO Reports</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Analysis Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Activity Report</td>
                                            <td><i class="fas fa-check"></i</td>                                
                                        </tr>
                                        <tr>
                                            <td>Monthly Action Plan</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Customer Support</td>
                                        </tr>
                                        <tr>
                                            <td>Email/ Chat/ Telephone</td>
                                            <td>On Request</td>                                
                                        </tr>
                                        <tr>
                                            <td>PT Client Center (24/7 Live Project Tracking)</td>
                                            <td>On Request</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Price</td>
                                        </tr>
                                        <tr>
                                            <td>Monthly Cost (LKR)</td>
                                            <td>80000</td>                                
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="smm-pricing-table mobile">
                                    <tbody>
                                        <tr class="header">
                                            <td></td>
                                            <td><h3>Gold</h3></td>
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Campaign Setup</td>
                                        </tr>
                                        <tr>
                                            <td>In-depth Site Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Content Duplicacy Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Initial Backlinks analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Google Penalty Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Mobile Usability Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Competition Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Keyword Research</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Targeted Keywords Count</td>
                                            <td>25</td>                               
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">On-Page Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>Title & Meta Tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Content Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Mobile site optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Page Speed Analysis & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>HTML Code Cleanup & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Internal Link Structuring & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Pages H tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Canonicalization/301 Redirect</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Schema Markup Implementation</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Image & Hyperlink Optimization</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr>
                                            <td>Robots.txt Creation/Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Sitemap Creation</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google & Bing Webmaster Tools Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Setup & Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Content Marketing and Link Acquisition (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Blog Articles</td>
                                            <td>4</td>                                
                                        </tr>
                                        <tr>
                                            <td>Informational Content Writing & Sharing</td>
                                            <td>4</td>                                
                                        </tr>
                                        <tr>
                                            <td>Guest Blog Outreach</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Magazine / News Placement</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Q&A Posting</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Premium PR Distribution</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Infographic Creation & Distribution</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Social Media Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Google Plus Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Twitter Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Instagram Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Youtube Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Monitoring and responses to social activity</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Unique Social Media Posting</td>
                                            <td>12</td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Ads Management</td>
                                            <td>200USD</td>                                
                                        </tr>
                                        <tr>
                                            <td>Google AdWords Management</td>
                                            <td>250USD</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Video Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Video / PPT Creation + Distribution</td>
                                            <td>1</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Local Search Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>NAP Syndication</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google My Business / Bing Local Listing</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Updating Pages & Schema Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Classified Submissions</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Coupon Distribution</td>
                                            <td>&nbsp;</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Monthly Reporting</td>
                                        </tr>
                                        <tr>
                                            <td>Search Engine Rank Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>SEO Reports</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Analysis Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Activity Report</td>
                                            <td><i class="fas fa-check"></i</td>                                
                                        </tr>
                                        <tr>
                                            <td>Monthly Action Plan</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Customer Support</td>
                                        </tr>
                                        <tr>
                                            <td>Email/ Chat/ Telephone</td>
                                            <td>On Request</td>                                
                                        </tr>
                                        <tr>
                                            <td>PT Client Center (24/7 Live Project Tracking)</td>
                                            <td>On Request</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Price</td>
                                        </tr>
                                        <tr>
                                            <td>Monthly Cost (LKR)</td>
                                            <td>135000</td>                                
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="smm-pricing-table mobile">
                                    <tbody>
                                        <tr class="header">
                                            <td></td>
                                            <td><h3>Platinum</h3></td>
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Campaign Setup</td>
                                        </tr>
                                        <tr>
                                            <td>In-depth Site Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Content Duplicacy Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Initial Backlinks analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Google Penalty Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Mobile Usability Check</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Competition Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Keyword Research</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Targeted Keywords Count</td>
                                            <td>50</td>                               
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">On-Page Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>Title & Meta Tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>Content Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Mobile site optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                 
                                        </tr>
                                        <tr>
                                            <td>Page Speed Analysis & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                               
                                        </tr>
                                        <tr>
                                            <td>HTML Code Cleanup & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                              
                                        </tr>
                                        <tr>
                                            <td>Internal Link Structuring & Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Pages H tags Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Canonicalization/301 Redirect</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Schema Markup Implementation</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Image & Hyperlink Optimization</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Robots.txt Creation/Analysis</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Sitemap Creation</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google & Bing Webmaster Tools Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Setup & Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Content Marketing and Link Acquisition (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Blog Articles</td>
                                            <td>8</td>                                
                                        </tr>
                                        <tr>
                                            <td>Informational Content Writing & Sharing</td>
                                            <td>6</td>                                
                                        </tr>
                                        <tr>
                                            <td>Guest Blog Outreach</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Magazine / News Placement</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Q&A Posting</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Premium PR Distribution</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Infographic Creation & Distribution</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Social Media Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Google Plus Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Page Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Twitter Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Instagram Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Youtube Account Setup</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Monitoring and responses to social activity</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Unique Social Media Posting</td>
                                            <td>15</td>                                
                                        </tr>
                                        <tr>
                                            <td>Facebook Ads Management</td>
                                            <td>450USD</td>                                
                                        </tr>
                                        <tr>
                                            <td>Google AdWords Management</td>
                                            <td>500USD</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Video Marketing (Per Month)</td>
                                        </tr>
                                        <tr>
                                            <td>Video / PPT Creation + Distribution</td>
                                            <td>3</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Local Search Optimization</td>
                                        </tr>
                                        <tr>
                                            <td>NAP Syndication</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google My Business / Bing Local Listing</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Updating Pages & Schema Integration</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Classified Submissions</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Coupon Distribution</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Monthly Reporting</td>
                                        </tr>
                                        <tr>
                                            <td>Search Engine Rank Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>SEO Reports</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Google Analytics Analysis Report</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr>
                                            <td>Activity Report</td>
                                            <td><i class="fas fa-check"></i</td>                                
                                        </tr>
                                        <tr>
                                            <td>Monthly Action Plan</td>
                                            <td><i class="fas fa-check"></i></td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Customer Support</td>
                                        </tr>
                                        <tr>
                                            <td>Email/ Chat/ Telephone</td>
                                            <td>On Request</td>                                
                                        </tr>
                                        <tr>
                                            <td>PT Client Center (24/7 Live Project Tracking)</td>
                                            <td>On Request</td>                                
                                        </tr>
                                        <tr class="sub-head">
                                            <td colspan="7">Price</td>
                                        </tr>
                                        <tr>
                                            <td>Monthly Cost (LKR)</td>
                                            <td>350000</td>                                
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-12"></div>
                        <div class="col-sm-12"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-cta">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h2 class="title">Ready to get started?</h2>    
                        </div>
                        <div class="col-md-8">
                            <p>We are looking forward to hear from you so don't hesitate to contact us.</p>
                            <a href="#" class="btn btn-white-outline">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('../../_partials/footer.php'); ?>     