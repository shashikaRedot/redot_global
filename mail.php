<?php
extract($_POST);
// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "Reply-To: ". $email . "\r\n";
$receiverEmail = "kavinda.silva@redot.lk";
$message = "
<html>
<body>
<p>Name: <strong>".$name."</strong></p>
<p>Subject: <strong>".$subject."</strong></p>
<p>Message: ".$message."</p>
</body>
</html>
";
try {
  mail($receiverEmail,$subject,$message,$headers);
  header("Location:../pages/contact.php?status=done");
} catch (\Throwable $th) {
  header("Location:../pages/contact.php?status=error");
}
?>